package jwt

type InvalidToken struct {
	s string
}

func (e *InvalidToken) Error() string {
	return e.s
}
func NewInvalidToken(text string) error {
	return &InvalidToken{text}
}
