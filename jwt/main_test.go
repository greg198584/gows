package jwt

import (
	"github.com/stretchr/testify/assert"
	"log"
	"os"
	"testing"
	"time"
)

var token string

func init() {
	//err := os.Setenv("CFG_PATH", "./__TEST__CI")
	//if err != nil {
	//	log.Fatal(err)
	//}
	_ = os.Remove("./jwt.key")
}
func TestSignCustom(t *testing.T) {
	s, err := SignUseKey("toto", map[string]interface{}{
		"id":  1,
		"iat": time.Now().Unix(),
	})
	//assert.Nil(t, err)
	assert.NotNil(t, s)
	t.Log(s)

	data, err := VerifyUseKey("toto", s)
	assert.Nil(t, err)
	t.Log(data)
}
func TestSign(t *testing.T) {
	s, err := Sign(map[string]interface{}{
		"id":  1,
		"iat": time.Now().Add(1 * time.Hour).Unix(),
	})
	assert.Nil(t, err)
	assert.NotNil(t, s)
	t.Log(s)
}
func TestSignAgain(t *testing.T) {
	Reset()
	var err error
	token, err = Sign(map[string]interface{}{
		"id":  2,
		"iat": time.Now().Add(1 * time.Hour).Unix(),
	})
	assert.Nil(t, err)
	assert.NotNil(t, token)
	t.Log(token)
}
func TestVerifyInvalidToken(t *testing.T) {
	_, err := Verify("")
	assert.NotNil(t, err)
	assert.IsType(t, &InvalidToken{}, err)
	t.Log(err)
}

//func TestVerify(t *testing.T) {
//	data, err := Verify(token)
//	assert.Nil(t, err)
//	t.Log(data)
//}

func BenchmarkSign(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var err error
		token, err = Sign(map[string]interface{}{
			"id":  1,
			"iat": time.Now().Unix(),
		})

		if err != nil {
			log.Fatal(err)
		}
	}
}

//func BenchmarkVerify(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		var err error
//		_, err = Verify(token)
//
//		if err != nil {
//			log.Fatal(err)
//		}
//	}
//}
