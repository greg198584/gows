package jwt

import (
	"bufio"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"gitlab.com/greg198584/gows/config"
	"os"
	"strings"
	"sync"
)

type JWT struct {
	key  *rsa.PrivateKey
	path string
}

var instance *JWT

func Reset() {
	instance = nil
}
func Sign(payload map[string]interface{}) (token string, err error) {
	return _Get()._Sign(payload)
}
func Verify(token string) (data map[string]interface{}, err error) {
	return _Get()._Verify(token)
}
func SignUseKey(key string, payload map[string]interface{}) (token string, err error) {
	return _GetFromKey(key)._Sign(payload)
}
func VerifyUseKey(key string, token string) (data map[string]interface{}, err error) {
	return _GetFromKey(key)._Verify(token)
}
func _Get() *JWT {
	if instance == nil {
		instance = new(JWT)
		jwt_path := config.GetValue("http", "jwt_path").(string)
		jwt_key := config.GetValue("http", "jwt_key").(string)
		instance.path = fmt.Sprintf("%s/%s", jwt_path, jwt_key)
	}
	return instance
}

var instanceMap = make(map[string]*JWT)
var instanceMapM = &sync.Mutex{}

func _GetFromKey(key string) *JWT {
	instanceMapM.Lock()
	defer instanceMapM.Unlock()

	_, f := instanceMap[key]
	if !f {
		jwt := new(JWT)
		jwt.path = fmt.Sprintf("%s/%s", config.GetValue("http", "jwt_path").(string), key)
		instanceMap[key] = jwt
	}
	return instanceMap[key]
}
func (j *JWT) _Sign(payload map[string]interface{}) (token string, err error) {
	err = j._GetKey(true)
	if err != nil {
		return
	}

	header := map[string]string{
		"typ": "jwt",
		"alg": "RS512",
	}
	h, err := json.Marshal(header)
	if err != nil {
		return
	}
	p, err := json.Marshal(payload)
	if err != nil {
		return
	}
	cryptHeader := base64.RawURLEncoding.EncodeToString(h)
	cryptPayload := base64.RawURLEncoding.EncodeToString(p)
	concat := cryptHeader + "." + cryptPayload
	rng := rand.Reader
	hashed := sha512.Sum512([]byte(concat))
	signature, err := rsa.SignPKCS1v15(rng, j.key, crypto.SHA512, hashed[:])
	if err != nil {
		return
	}
	cryptSignature := base64.RawURLEncoding.EncodeToString(signature)
	token = concat + "." + cryptSignature
	return
}
func (j *JWT) _GenerateKey() (err error) {
	j.key, err = rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return
	}
	return
}
func (j *JWT) _Save() (err error) {
	pemPrivateFile, err := os.Create(fmt.Sprintf("%s", j.path))
	if err != nil {
		return err
	}
	var pemPrivateBlock = &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(j.key),
	}
	err = pem.Encode(pemPrivateFile, pemPrivateBlock)
	if err != nil {
		return
	}
	err = pemPrivateFile.Close()

	asn1Bytes, err := asn1.Marshal(j.key.PublicKey)
	if err != nil {
		return err
	}

	var pemkey = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: asn1Bytes,
	}

	pemfile, err := os.Create(fmt.Sprintf("%s.pub", j.path))
	if err != nil {
		return err
	}
	defer pemfile.Close()

	err = pem.Encode(pemfile, pemkey)
	if err != nil {
		return err
	}
	return
}
func (j *JWT) _Verify(token string) (data map[string]interface{}, err error) {
	err = j._GetKey(false)
	if err != nil {
		return
	}
	tokens := strings.Split(token, ".")
	if len(tokens) != 3 {
		err = NewInvalidToken("incorrect value")
		return
	}
	var payload []byte
	signature, err := base64.RawURLEncoding.DecodeString(tokens[2])
	if err != nil {
		return
	}
	payload, err = base64.RawURLEncoding.DecodeString(tokens[1])
	if err != nil {
		return
	}
	concat := tokens[0] + "." + tokens[1]
	hashed := sha512.Sum512([]byte(concat))
	err = rsa.VerifyPKCS1v15(&j.key.PublicKey, crypto.SHA512, hashed[:], signature)
	if err != nil {
		return
	}
	err = json.Unmarshal(payload, &data)
	if err != nil {
		return
	}
	return
}
func (j *JWT) _GetKey(generate bool) (err error) {
	if j.key != nil {
		return
	}
	_, err = os.Stat(j.path)
	if os.IsNotExist(err) && generate {
		err = j._GenerateKey()
		if err != nil {
			return
		}
		err = j._Save()
		if err != nil {
			return
		}
		return
	}
	privateKeyFile, err := os.Open(j.path)
	if err != nil {
		return
	}

	pemfileinfo, _ := privateKeyFile.Stat()
	var size = pemfileinfo.Size()
	pembytes := make([]byte, size)
	buffer := bufio.NewReader(privateKeyFile)
	_, err = buffer.Read(pembytes)
	if err != nil {
		return
	}
	data, _ := pem.Decode(pembytes)

	err = privateKeyFile.Close()
	if err != nil {
		return
	}
	j.key, err = x509.ParsePKCS1PrivateKey(data.Bytes)
	if err != nil {
		return
	}
	return
}
