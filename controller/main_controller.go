package controller

import (
	"context"
	"database/sql"
	"gitlab.com/greg198584/gows/db"
	"gitlab.com/greg198584/gows/logger"
)

const NAMESPACE = "main_controller"

type MainController struct {
	Name       string
	Connector  db.Connector
	Connectors db.Connectors
	Conn       *db.Conn
	Tx         *db.Tx
	Results    *sql.Rows
}

func (mc *MainController) CloseConnexion() (err error) {
	if mc.Conn != nil {
		logger.GetLog(NAMESPACE).Debug("Close Conn")
		err = mc.Conn.Close()
		if err != nil {
			logger.GetLog(NAMESPACE).Error(err.Error())
			return
		}
	}
	if mc.Tx != nil {
		logger.GetLog(NAMESPACE).Debug("RollbackTransaction")
		err = mc.RollbackTransaction()
		if err != nil {
			logger.GetLog(NAMESPACE).Error(err.Error())
			return
		}
	}
	if mc.Results != nil {
		logger.GetLog(NAMESPACE).Debug("Close Results")
		err = mc.Results.Close()
		if err != nil {
			logger.GetLog(NAMESPACE).Error(err.Error())
			return
		}
	}
	return
}
func (mc *MainController) GetCnxMaster() (err error) {
	if mc.Conn == nil {
		mc.Conn, err = mc.Connectors.Master.GetCnx()
	}
	return
}
func (mc *MainController) GetCnxSlave() (err error) {
	if mc.Conn == nil {
		mc.Conn, err = mc.Connectors.Slave.GetCnx()
	}
	return
}
func (mc *MainController) CommitTransaction() (err error) {
	err = mc.Tx.Commit()
	if err != nil {
		return
	}
	mc.Tx = nil
	return
}
func (mc *MainController) RollbackTransaction() (err error) {
	err = mc.Tx.Rollback()
	if err != nil {
		return
	}
	return
}
func (mc *MainController) BeginTransactionMaster(ctx context.Context) (err error) {
	mc.Conn, err = mc.Connectors.Master.GetCnx()
	if err != nil {
		return
	}
	mc.Tx, err = mc.Conn.BeginTx(ctx, nil)
	if err != nil {
		return
	}
	return
}
func (mc *MainController) BeginTransactionSlave(ctx context.Context) (err error) {
	mc.Conn, err = mc.Connectors.Slave.GetCnx()
	if err != nil {
		return
	}
	mc.Tx, err = mc.Conn.BeginTx(ctx, nil)
	if err != nil {
		return
	}
	return
}
