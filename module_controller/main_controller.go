package module_controller

import (
	"gitlab.com/greg198584/gows/controller"
	"gitlab.com/greg198584/gows/db"
	"gitlab.com/greg198584/gows/router"
	"gitlab.com/greg198584/gows/tools"
)

type MainController struct {
	*router.Context
	M controller.MainController
}

func (mc *MainController) OnCreate() (err error) {
	mc.M.Connectors, err = db.Init(mc)
	return
}
func (mc *MainController) OnFinish() (err error) {
	mc.M.CloseConnexion()
	return
}
func (mc *MainController) GetUserID() *int64 {
	JwtData := mc.GetContextData("Jwt").(map[string]interface{})
	return tools.ToInt(JwtData["id"])
}
func (mc *MainController) RetError(StatusCode int, Msg string) {
	mc.JSON(StatusCode, router.SetError(StatusCode, Msg))
	return
}
