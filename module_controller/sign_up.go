package module_controller

import (
	"gitlab.com/greg198584/gows/controller"
	"gitlab.com/greg198584/gows/logger"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"net/mail"
)

type User struct {
	Email    string `json:"email"`
	Nom      string `json:"nom"`
	Prenom   string `json:"prenom"`
	Password string `json:"password"`
}

type SignUpController struct {
	*MainController
}

func (suc *MainController) SignUpController() *SignUpController {
	return new(SignUpController)
}
func _validMailAddress(address string) (string, bool) {
	addr, err := mail.ParseAddress(address)
	if err != nil {
		return "", false
	}
	return addr.Address, true
}
func (suc *SignUpController) SignUp(payload struct {
	Body User `type:"json" validate:"required"`
}) (err error) {
	if mailAddress, ok := _validMailAddress(payload.Body.Email); ok {
		err = suc.M.GetCnxMaster()
		if err != nil {
			suc.RetError(http.StatusBadRequest, err.Error())
			return
		}
		password, errPassword := suc.HashPassword(payload.Body.Password)
		if errPassword != nil {
			suc.RetError(http.StatusBadRequest, err.Error())
			return
		} else {
			query := `
INSERT INTO user SET 
	nom = ?,
	prenom = ?,
	password = ?,
	email = ?;`
			res, err := suc.M.Conn.ExecContext(suc.Context, query,
				payload.Body.Nom,
				payload.Body.Prenom,
				password,
				mailAddress,
			)
			if err != nil {
				logger.GetLog(controller.NAMESPACE).Error(err.Error())
				suc.RetError(http.StatusBadRequest, err.Error())
			}
			var errLastInsertId error
			var id int64
			if id, errLastInsertId = res.LastInsertId(); id != 0 {
				suc.JSON(http.StatusCreated, "user created")
			} else {
				if errLastInsertId != nil {
					logger.GetLog(controller.NAMESPACE).Error(err.Error())
					suc.RetError(http.StatusBadRequest, "error create user")
				}
			}
		}
	}
	return
}
func (suc *SignUpController) HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}
