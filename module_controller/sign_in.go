package module_controller

import (
	"gitlab.com/greg198584/gows/jwt"
	"gitlab.com/greg198584/gows/logger"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"time"
)

type SignInController struct {
	*MainController
}

type SignInData struct {
	UserID   int64
	Password string
	Actif    bool
}

type Result struct {
	Token string `json:"token"`
}

type ResultError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func (sic *MainController) SignInController() *SignInController {
	return new(SignInController)
}

func (sic *SignInController) SignIn(payload struct {
	Body struct {
		Email    string `json:"email" validate:"required"`
		Password string `json:"password" validate:"required"`
	} `type:"json" validate:"required"`
}) (err error) {
	if payload.Body.Email == "" || payload.Body.Password == "" {
		sic.JSON(http.StatusBadRequest, ResultError{
			Code:    http.StatusBadRequest,
			Message: "Le Login et le mot de passe sont requis",
		})
	} else {
		err = sic.M.GetCnxMaster()
		sic.M.Results, err = sic.M.Conn.QueryContext(sic.Context, `
SELECT
	user_id,
	password
FROM
	user
WHERE
	email = ?`, payload.Body.Email)

		if err != nil {
			return err
		}
		var signInData SignInData
		for sic.M.Results.Next() {
			err = sic.M.Results.Scan(
				&signInData.UserID,
				&signInData.Password,
			)
			if err != nil {
				logger.GetLog("userController").Error(err.Error())
				return err
			}
		}
		if sic.CheckPasswordHash(payload.Body.Password, signInData.Password) {
			var res Result
			res.Token, err = jwt.Sign(map[string]interface{}{
				"user_id": signInData.UserID,
				"email":   payload.Body.Email,
				"iat":     time.Now().Add(2 * time.Hour).Unix(),
			})
			sic.JSON(http.StatusOK, res)
		} else {
			sic.JSON(http.StatusUnauthorized, ResultError{
				Code:    http.StatusUnauthorized,
				Message: "login ou mot de passe incorrect",
			})
		}
	}
	return
}

func (sic *SignInController) HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func (sic *SignInController) CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
