package spool

import (
	"math"
	"reflect"
	"time"
)

type spool struct {
	Start       time.Time
	concurrency int
	Position    int
	callData    func(chan bool, int, interface{})
	callEnd     func()
	data        []interface{}
}

func NewSpool() *spool {
	return &spool{}
}

func interfaceSlice(slice interface{}) []interface{} {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		panic("InterfaceSlice() given a non-slice type")
	}

	ret := make([]interface{}, s.Len())
	for i := 0; i < s.Len(); i++ {
		ret[i] = s.Index(i).Interface()
	}

	return ret
}

func (spool *spool) Run(data interface{}, concurrency int) {
	spool.data = interfaceSlice(data)
	spool.concurrency = concurrency
	spool.process()
}

func (spool *spool) OnData(call func(chan bool, int, interface{})) *spool {
	spool.callData = call

	return spool
}
func (spool *spool) OnFinish(call func()) *spool {
	spool.callEnd = call

	return spool
}

func (spool *spool) Increment() *spool {
	spool.Position++

	return spool
}

func (spool *spool) process() {
	var start = 0
	var end = spool.concurrency
	var c = float64(len(spool.data)) / float64(end)
	var max = int(math.Ceil(float64(c)))

	if spool.Position >= 1 {
		start = spool.Position * spool.concurrency
		end = (spool.Position + 1) * end
	}

	if spool.Position+1 == max {
		start = spool.Position * spool.concurrency
		end = len(spool.data)
	}

	if spool.Position == max {
		spool.callEnd()
		return
	}
	current := spool.data[start:end]
	status := make(chan bool, len(current))
	for pos, element := range current {
		go spool.callData(status, start+pos, element)
	}

	for i := 0; i < len(current); i++ {
		select {
		case <-status:
		}
	}

	spool.Increment().process()
}
