#!/bin/bash

go mod tidy

PACKAGE_DIR=`pwd`
echo "Running tests for $PACKAGE_DIR"
for directory in $(ls -d */)
do
    directory=$(echo "$directory" | rev | cut -c 2- | rev)
    CURRENT_DIR="${PACKAGE_DIR}/$directory"
    cd "$CURRENT_DIR"
    echo "$CURRENT_DIR"

    if ! find . -name "*_test.go" | grep . &>/dev/null; then
        echo -e "No test file, skipping\n"
        continue
    fi

    CFG_PATH=""
    if [[ -d "${CURRENT_DIR}/__TEST_CI" ]]; then
        CFG_PATH="${CURRENT_DIR}/__TEST_CI"
    elif [[ -d "${CURRENT_DIR}/config" ]]; then
        CFG_PATH="${CURRENT_DIR}/config"
    fi
    echo "CFG_PATH=$CFG_PATH"

    env ENV=TEST CFG_PATH="$CFG_PATH" gotestsum --format testname 2>/dev/null
    if [[ "$?" != "0" ]]; then
        exit 1
    fi

    echo
done

exit 0