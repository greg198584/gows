module gitlab.com/greg198584/gows

go 1.18

require (
	github.com/getsentry/sentry-go v0.16.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.8.0
	github.com/twinj/uuid v1.0.0
	golang.org/x/crypto v0.0.0-20221012134737-56aed061732a
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/myesui/uuid v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220928140112-f11e5e49a4ec // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/stretchr/testify.v1 v1.2.2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
