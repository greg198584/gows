package promise

import (
	"context"
	"runtime"
	"runtime/debug"
	"sync"
)

const (
	pending   = iota
	completed = 1
	rejected  = 2
)

type Promise struct {
	caller   *runtime.Func
	called   bool
	state    int
	executor func(resolve func(interface{}), reject func(error))
	then     []func(data interface{}) interface{}
	catch    []func(err error) error
	result   interface{}
	err      error
	mutex    *sync.Mutex
	context  context.Context
	wg       *sync.WaitGroup
	async    bool
}

func New(context context.Context, executor func(resolve func(interface{}), reject func(error))) *Promise {
	var Promise = &Promise{
		state:    pending,
		executor: executor,
		called:   false,
		then:     make([]func(interface{}) interface{}, 0),
		catch:    make([]func(error) error, 0),
		result:   nil,
		err:      nil,
		mutex:    &sync.Mutex{},
		wg:       &sync.WaitGroup{},
		context:  context,
	}
	Promise.add()
	return Promise
}

func Serie(Promises ...*Promise) *Promise {
	psLen := len(Promises)
	if psLen == 0 {
		return Resolve(context.Background(), make([]interface{}, 0))
	}
	ctx := Promises[0].context
	p := New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolutionsChan := make(chan []interface{}, psLen)
		errorChan := make(chan error, psLen)
		var er error
		for index, Promise := range Promises {
			if er != nil {
				errorChan <- er
				continue
			}
			if Promise.isCanceled() {
				errorChan <- ctx.Err()
				continue
			}
			func(i int) {
				Promise.Then(func(data interface{}) interface{} {
					resolutionsChan <- []interface{}{i, data}
					return data
				}).Catch(func(err error) error {
					er = err
					errorChan <- err
					return err
				})
			}(index)
		}
		var err error
		resolutions := make([]interface{}, psLen)
		for x := 0; x < psLen; x++ {
			select {
			case resolution := <-resolutionsChan:
				resolutions[resolution[0].(int)] = resolution[1]
				break
			case err = <-errorChan:
				break
			}
		}
		if err != nil {
			reject(err)
			return
		}
		resolve(resolutions)
	})

	p.call()
	_, _ = p.await()
	return p
}

func Parallel(Promises ...*Promise) *Promise {
	psLen := len(Promises)
	if psLen == 0 {
		return Resolve(context.Background(), make([]interface{}, 0))
	}
	ctx := Promises[0].context
	p := New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolutionsChan := make(chan []interface{}, psLen)
		errorChan := make(chan error, psLen)
		for index, Promise := range Promises {
			if Promise.isCanceled() {
				errorChan <- ctx.Err()
				continue
			}
			Promise := Promise
			go func(i int) {
				Promise.Then(func(data interface{}) interface{} {
					resolutionsChan <- []interface{}{i, data}
					return data
				}).Catch(func(err error) error {
					errorChan <- err
					return err
				})
			}(index)
		}
		resolutions := make([]interface{}, psLen)
		var err error
		for x := 0; x < psLen; x++ {
			select {
			case resolution := <-resolutionsChan:
				resolutions[resolution[0].(int)] = resolution[1]
				break
			case err = <-errorChan:
				break
			}
		}
		if err != nil {
			reject(err)
			return
		}
		resolve(resolutions)
	})
	p.call()
	_, _ = p.await()
	return p
}
func (p *Promise) Then(data func(data interface{}) interface{}) (new *Promise) {
	if p.err != nil {
		return p
	}
	if !p.called {
		p.call()
		for p.state == pending {
		}
		return p.Then(data)
	}
	p.mutex.Lock()
	defer p.mutex.Unlock()

	switch p.state {
	case completed:
		switch result := p.result.(type) {
		case *Promise:
			result.call()
			res, err := result.await()
			if err != nil {
				p.err = p.convertErr(err)
				p.state = rejected

				break
			}
			defer func() {
				var r = recover()
				if r != nil {
					p.err = p.convertErr(r)
					p.state = rejected
					new = p
				}
			}()
			p.result = data(res)
		default:
			defer func() {
				var r = recover()
				if r != nil {
					p.err = p.convertErr(r)
					p.state = rejected
					new = p
				}
			}()
			p.result = data(p.result)
		}
		break
	}
	return p
}
func (p *Promise) Catch(rejection func(err error) error) *Promise {
	switch result := p.result.(type) {
	case *Promise:
		result.call()
		_, err := result.await()
		if err != nil {
			p.err = p.convertErr(err)
			p.state = rejected
			break
		}
	case error:
		p.err = result
		p.state = rejected
	}
	p.mutex.Lock()
	defer p.mutex.Unlock()

	switch p.state {
	case rejected:
		p.err = rejection(p.err)
	}
	return p
}
func (p *Promise) Result() (interface{}, error) {
	return p.result, p.err
}
func (p *Promise) HasError() bool {
	return p.err != nil
}
func (p *Promise) Await() (result interface{}, err error) {
	p.Then(func(data interface{}) interface{} {
		result = data
		return nil
	}).Catch(func(e error) error {
		err = e
		return e
	})
	return
}
func Resolve(context context.Context, resolution interface{}) *Promise {
	return New(context, func(resolve func(interface{}), reject func(error)) {
		resolve(resolution)
	})
}
func Reject(context context.Context, err error) *Promise {
	return New(context, func(resolve func(interface{}), reject func(error)) {
		reject(err)
	})
}

func (p *Promise) call() {
	defer p.handlePanic()
	p.called = true
	p.executor(p.resolve, p.reject)
}
func (p *Promise) convertErr(err interface{}) (res error) {
	switch result := err.(type) {
	case error:
		res = result
		break
	case string:
		res = NewInvokeError(result, string(debug.Stack()))
		break
	}
	return
}
func (p *Promise) resolve(resolution interface{}) {
	p.mutex.Lock()

	if p.state != pending {
		p.mutex.Unlock()
		return
	}
	switch result := resolution.(type) {
	case *Promise:
		result.call()
		res, err := result.await()
		if err != nil {
			p.mutex.Unlock()
			p.reject(err)
			return
		}
		p.result = res
	default:
		p.result = result
	}
	p.finish()
	p.state = completed
	p.mutex.Unlock()
}
func (p *Promise) reject(err error) {
	p.mutex.Lock()
	if p.state != pending {
		return
	}
	p.err = err
	p.finish()
	p.state = rejected
	p.mutex.Unlock()
}
func (p *Promise) await() (interface{}, error) {
	p.wg.Wait()
	return p.result, p.err
}
func (p *Promise) isCanceled() bool {
	return p.context.Err() == context.Canceled
}
func (p *Promise) add() {
	p.wg.Add(1)
}
func (p *Promise) finish() {
	p.wg.Done()
}
func (p *Promise) handlePanic() {
	var r = recover()
	if r != nil {
		p.reject(p.convertErr(r))
	}
}
