package promise

import (
	"strings"
)

func NewInvokeError(text string, trace string) error {
	stack := strings.Replace(trace, "\t", "", -1)
	stackSlice := strings.Split(stack, "\n")
	return &InvokeError{text, stackSlice}
}

type InvokeError struct {
	s     string
	trace []string
}

func (e *InvokeError) Trace() []string {
	return e.trace
}
func (e *InvokeError) Error() string {
	return e.s
}
