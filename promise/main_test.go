package promise

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"net/http"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	ctx := context.Background()
	var res interface{}
	var promise = New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve("Hello")
	}).Then(func(data interface{}) interface{} {
		res = data
		t.Logf("result:%v", data)
		return nil
	})
	assert.NotNil(t, promise)
	assert.NotNil(t, res)
	t.Log("Instance of promise is not null")
}

func TestPromise_Then(t *testing.T) {
	ctx := context.Background()
	var p = New(ctx, func(resolve func(interface{}), reject func(error)) {
		const sum = 2 + 2
		resolve(sum)
	})
	p.Then(func(data interface{}) interface{} {
		fmt.Println("The result is:", data)
		return data.(int) + 1
	})
	p.Then(func(data interface{}) interface{} {
		fmt.Println("The new result is:", data)
		return nil
	})
}
func TestPromiseSubResolve(t *testing.T) {
	ctx := context.Background()
	var p = New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve(New(ctx, func(resolve func(interface{}), reject func(error)) {
			const sum = 2 + 2
			resolve(sum)
		}))
	})
	p.Then(func(data interface{}) interface{} {
		fmt.Println("The result is:", data)
		return nil
	})
}
func TestPromiseSubReject(t *testing.T) {
	ctx := context.Background()
	New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve(New(ctx, func(resolve func(interface{}), reject func(error)) {
			reject(errors.New("fail"))
		}))
	}).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return New(ctx, func(resolve func(interface{}), reject func(error)) {
			resolve("test")
		}).Catch(func(error error) error {
			log.Print(error)
			return error
		})
	}).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	})
}
func TestPromiseSubSubResolve(t *testing.T) {
	ctx := context.Background()
	New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve("test")
	}).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return data
	}).Then(func(data interface{}) interface{} {
		return nil
	})
}
func TestNewSerieCancelContext(t *testing.T) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	cancel()
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		time.Sleep(100 * time.Millisecond)
		resolve("cancel")
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve("cancel")
	})

	Serie(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		t.Logf("result:%v", error)
		return nil
	})
}
func TestNewParallelCancelContext(t *testing.T) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	cancel()
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("wait 100ms before resolve in last")
		time.Sleep(100 * time.Millisecond)
		resolve("last")
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("first")
		resolve("first resolve")
	})

	Parallel(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		t.Logf("result:%v", error)
		return nil
	})
}
func TestNewSerie(t *testing.T) {
	ctx := context.Background()
	var err error
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("wait 100ms before resolve in last")
		time.Sleep(100 * time.Millisecond)
		resolve("first")
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("last resolve before use serie")
		resolve("last")
	})
	Serie(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	assert.Nil(t, err)
}
func TestNewSerieAwait(t *testing.T) {
	ctx := context.Background()
	var err error
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("wait 100ms before resolve in last")
		time.Sleep(100 * time.Millisecond)
		resolve("first")
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("last resolve before use serie")
		resolve("last")
	})
	result, err := Serie(p1, p2).Await()
	t.Logf("%v", result)
	assert.Nil(t, err)
}
func TestNewSerieAwaitReject(t *testing.T) {
	ctx := context.Background()
	var err error
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("wait 100ms before resolve in last")
		time.Sleep(100 * time.Millisecond)
		resolve("first")
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("last resolve before use serie")
		reject(errors.New("reject"))
	})
	result, err := Serie(p1, p2).Await()
	t.Logf("%v", result)
	assert.NotNil(t, err)
}
func TestNewSerieWithReject(t *testing.T) {
	ctx := context.Background()
	var err error
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("wait 100ms before resolve in last")
		time.Sleep(100 * time.Millisecond)
		resolve("first")
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("last resolve before use serie")
		reject(errors.New("reject"))
	})

	Serie(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	assert.NotNil(t, err)
}
func TestNewSerieWithPanic(t *testing.T) {
	ctx := context.Background()
	var err error
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("wait 100ms before resolve in last")
		time.Sleep(100 * time.Millisecond)
		panic("reject")
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("last resolve before use serie")
		resolve("second")
	})

	Serie(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	assert.NotNil(t, err)
	if assert.IsType(t, err, &InvokeError{}) {
		assert.NotNil(t, err.(*InvokeError).Trace())
	}
}
func TestNewSerieWithSerieEmpty(t *testing.T) {
	var err error
	Serie().Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	assert.Nil(t, err)
}
func TestNewParallelEmpty(t *testing.T) {
	var err error
	Parallel().Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	assert.Nil(t, err)
}
func TestNewParallel(t *testing.T) {
	ctx := context.Background()
	var err error
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("wait 100ms before resolve in last")
		time.Sleep(100 * time.Millisecond)
		resolve("first")
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("last resolve before use serie")
		reject(errors.New("reject"))
	})

	Parallel(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	log.Print(err)
}
func TestNewParallelPanic(t *testing.T) {
	ctx := context.Background()
	var err error
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("wait 100ms before resolve in last")
		time.Sleep(1 * time.Second)
		resolve("first")
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Logf("apnic")
		panic("panic")
	})

	Parallel(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	assert.NotNil(t, err)
	t.Logf("receive error -> %s", err.Error())
}
func TestNewParallelResolve(t *testing.T) {
	ctx := context.Background()
	var err error
	var p1 = Resolve(ctx, "first")
	var p2 = Resolve(ctx, "last")
	Parallel(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	assert.Nil(t, err)
}
func TestNewParallelReject(t *testing.T) {
	ctx := context.Background()
	var err error
	var p1 = Resolve(ctx, "first")
	var p2 = Reject(ctx, errors.New("last error"))
	Parallel(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	assert.NotNil(t, err)
	t.Logf("receive error -> %s", err.Error())
}
func parseJSON(data []byte) *Promise {
	ctx := context.Background()
	return New(ctx, func(resolve func(interface{}), reject func(error)) {
		var body = make(map[string]string)

		err := json.Unmarshal(data, &body)
		if err != nil {
			reject(err)
			return
		}
		resolve(body)
	})
}
func TestPromiseHttpChainingReject(t *testing.T) {
	ctx := context.Background()
	var requestPromise = New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve(nil)
	})
	requestPromise.
		Then(func(data interface{}) interface{} {
			return Reject(ctx, errors.New("reject error"))
		}).
		Then(func(data interface{}) interface{} {
			var body = data.(map[string]string)
			t.Log("[Inside Promise] Origin:", body["origin"])

			return body
		}).
		Catch(func(error error) error {
			t.Log(error.Error())
			return error
		})

	_, err := requestPromise.Result()
	assert.NotNil(t, err)
}
func TestPromiseHttpChainingPanic(t *testing.T) {
	ctx := context.Background()
	var requestPromise = New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve(nil)
	})
	requestPromise.
		Then(func(data interface{}) interface{} {
			panic("panic")
		}).
		Then(func(data interface{}) interface{} {
			var body = data.(map[string]string)
			fmt.Println("[Inside Promise] Origin:", body["origin"])

			return body
		}).
		Catch(func(error error) error {
			fmt.Println(error)
			assert.NotNil(t, error)
			return error
		})

	_, err := requestPromise.Result()
	log.Print(err)
	assert.NotNil(t, err)
}
func TestPromiseHttpChainingPanicSub(t *testing.T) {
	ctx := context.Background()
	var requestPromise = New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve(nil)
	})
	requestPromise.
		Then(func(data interface{}) interface{} {
			return nil
		}).
		Then(func(data interface{}) interface{} {
			panic("fail")
		}).
		Catch(func(error error) error {
			fmt.Println(error.Error())
			return error
		})

	_, err := requestPromise.Result()
	log.Print(err)
	assert.NotNil(t, err)
}
func TestPromiseHttpChainingRejectSub(t *testing.T) {
	ctx := context.Background()
	var requestPromise = New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve(nil)
	})
	requestPromise.
		Then(func(data interface{}) interface{} {
			panic("err")
		}).
		Then(func(data interface{}) interface{} {
			return Reject(ctx, errors.New("fail"))
		}).
		Catch(func(error error) error {
			fmt.Println(error.Error())
			return error
		})

	_, err := requestPromise.Result()
	assert.NotNil(t, err)
}
func TestPromiseHttpChainingErrorSub(t *testing.T) {
	ctx := context.Background()
	var requestPromise = New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve(nil)
	})
	requestPromise.
		Then(func(data interface{}) interface{} {
			return nil
		}).
		Then(func(data interface{}) interface{} {
			return errors.New("test")
		}).
		Catch(func(error error) error {
			assert.NotNil(t, error)
			return error
		})

	_, err := requestPromise.Result()
	assert.NotNil(t, err)
}
func TestPromiseHttpChainingRejectPanicSub(t *testing.T) {
	ctx := context.Background()
	var requestPromise = New(ctx, func(resolve func(interface{}), reject func(error)) {
		resolve(nil)
	})
	requestPromise.
		Then(func(data interface{}) interface{} {
			return nil
		}).
		Then(func(data interface{}) interface{} {
			panic(errors.New("test"))
		}).
		Catch(func(error error) error {
			fmt.Println(error.Error())
			return error
		})

	_, err := requestPromise.Result()
	assert.True(t, requestPromise.HasError())
	assert.NotNil(t, err)
}
func TestPromiseHttpChainingResolve(t *testing.T) {
	ctx := context.Background()
	var requestPromise = New(ctx, func(resolve func(interface{}), reject func(error)) {
		var url = "https://httpbin.org/ip"

		resp, err := http.Get(url)
		if err != nil {
			reject(err)
			return
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			reject(err)
			return
		}

		resolve(body)
	})
	var pass = false
	requestPromise.
		Then(func(data interface{}) interface{} {
			return parseJSON(data.([]byte))
		}).
		Then(func(data interface{}) interface{} {
			var body = data.(map[string]string)
			fmt.Println("[Inside Promise] Origin:", body["origin"])
			pass = true
			return body
		}).
		Catch(func(error error) error {
			fmt.Println(error.Error())
			return nil
		})

	value, err := requestPromise.Result()
	assert.Nil(t, err)
	assert.True(t, pass)
	origin := value.(map[string]string)["origin"]
	fmt.Println("[Outside Promise] Origin: " + origin)
}

func BenchmarkNew(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ctx := context.Background()
		New(ctx, func(resolve func(interface{}), reject func(error)) {
			resolve("Hello world")
		})
	}
}
func BenchmarkNewReject(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ctx := context.Background()
		New(ctx, func(resolve func(interface{}), reject func(error)) {
			reject(errors.New("reject"))
		})
	}
}
func BenchmarkNewSerie(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ctx := context.Background()
		var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
			resolve("first")
		})
		var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
			resolve("last")
		})
		_, _ = Serie(p1, p2).Await()
	}
}

func TestNewSerieDebug(t *testing.T) {
	ctx := context.Background()
	second := false
	var err error
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("First")

		go func() {
			time.Sleep(2 * time.Second)
			resolve("first")
			assert.False(t, second)
		}()
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("Second")
		second = true
		resolve("Second")
	})

	_, err = Serie(p1, p2).Await()
	if err != nil {
		log.Print(err)
	}
	assert.Nil(t, err)
}
func TestNewSerieDebug2(t *testing.T) {
	ctx := context.Background()
	var err error
	// second := false
	var p1 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		go func() {
			time.Sleep(2 * time.Second)
			resolve("first")
			//			assert.True(t, second)
		}()
	})
	var p2 = New(ctx, func(resolve func(interface{}), reject func(error)) {
		t.Log("Second")
		resolve("Second")
	})

	Parallel(p1, p2).Then(func(data interface{}) interface{} {
		t.Logf("result:%v", data)
		return nil
	}).Catch(func(error error) error {
		err = error
		return nil
	})
	assert.Nil(t, err)
}
