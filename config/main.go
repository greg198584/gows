package config

import (
	"encoding/json"
	"fmt"
	"gitlab.com/greg198584/gows/tools"
	"io/ioutil"
	"log"
	"os"
	"sync"
)

var cache = new(sync.Map)

type payload struct {
	path  string
	isDir bool
	v     interface{}
}

func Get(name string, v interface{}) (err error) {
	_, err = _GetInstance(name, &v)
	return
}
func GetPath() string {
	return os.Getenv("CFG_PATH")
}
func Reset() {
	cache = new(sync.Map)
}
func GetValue(configNameSpace string, field string) interface{} {
	var test interface{}
	pay, err := _GetInstance(configNameSpace, test)
	if err != nil {
		log.Printf(fmt.Sprintf("err: %s", err.Error()))
		return nil
	}
	res := tools.GetExtractor(pay.v, field)
	return res
}
func GetDefaultIntValue(configNameSpace string, field string, defaultValue int64) int64 {
	value := GetIntValue(configNameSpace, field)

	if value == nil {
		return defaultValue
	}

	return *value
}
func GetIntValue(configNameSpace string, field string) *int64 {
	var test interface{}
	pay, err := _GetInstance(configNameSpace, test)

	if err != nil {
		return nil
	}
	res := tools.GetExtractor(pay.v, field)
	i := tools.ToInt(res)

	if i == nil {
		return nil
	}
	return i
}
func _GetInstance(name string, v interface{}) (p *payload, err error) {
	pay, ok := cache.Load(name)
	if ok {
		p = pay.(*payload)
		p.v = v
		err = p._GetFile()
		return
	}
	p = new(payload)
	p.isDir = false
	p.path = GetPath() + "/" + name
	p.v = v
	err = p._GetFile()

	if err == nil {
		cache.Store(name, p)
	}
	return
}
func (p *payload) _Open(filename string) (file *os.File, err error) {
	file, err = os.Open(filename)
	if err != nil {
		return
	}
	return
}
func (p *payload) _GetFile() (err error) {
	if !p.isDir {
		err = p._Decode(p.path + ".json")
		if err != nil {
			return
		}
	}
	return
}
func (p *payload) _Decode(filename string) (err error) {
	file, err := p._Open(filename)
	if err != nil {
		return
	}
	byteValue, err := ioutil.ReadAll(file)
	_ = file.Close()
	err = json.Unmarshal(byteValue, &p.v)
	if err != nil {
		return
	}
	return
}
