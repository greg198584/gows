FROM golang:1.18
WORKDIR /go/app
COPY . .
RUN go install gotest.tools/gotestsum@latest \
    && go mod tidy
ENTRYPOINT ["/go/app/test.sh"]
