package logger

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"runtime"
	"strings"
	"sync"
	"time"
)

type logger struct {
	UUID        string
	namespaces  sync.Map
	filter      []string
	levelFilter []string
	path        string
	level       string
	verbose     bool
}
type namespace struct {
	name string
}

var instance *logger
var pid int

func init() {
	pid = os.Getpid()
}

func Load(filter string, level string, verbose bool) {
	log.SetFlags(0)
	instance = &logger{}
	if filter != "" {
		filter = strings.ToUpper(filter)
		instance.filter = strings.Split(strings.Replace(filter, " ", "", -1), ",")
	}
	if level != "" {
		level = strings.ToUpper(level)
		instance.levelFilter = strings.Split(strings.Replace(level, " ", "", -1), ",")
	}
	instance.verbose = verbose
}

func SetUUID(uuid string) {
	instance.UUID = uuid
	return
}
func GetLog(Namespace ...string) *namespace {
	if instance == nil {
		instance = &logger{}
	}

	concatNamespace := strings.Join(Namespace, ".")
	h, ok := instance.namespaces.Load(concatNamespace)
	if ok {
		n := h.(namespace)
		return &n
	}

	n := namespace{name: strings.ToUpper(concatNamespace)}
	instance.namespaces.Store(concatNamespace, n)

	return &n
}

func (n *namespace) _IsHidden(level string) bool {
	if !_Contains(instance.levelFilter, level) {
		return true
	}
	if len(instance.filter) == 0 {
		return true
	}
	// On filtre le namespace
	status := true
	for _, filter := range instance.filter {
		if filter == "*" {
			filter = " *"
		}
		var re = regexp.MustCompile(`(?m)` + filter)
		found := re.FindAllString(n.name, -1)
		if len(found) > 0 {
			status = false
			break
		}
	}
	return status
}
func (n *namespace) _Print(level string, message string) {
	if n._IsHidden(level) {
		return
	}
	pc, _, line, _ := runtime.Caller(2)
	name := runtime.FuncForPC(pc).Name()
	split := strings.Split(name, "/")
	packageMethodName := split[len(split)-1]

	outputString := fmt.Sprintf("%s", time.Now().Format("2006-01-02T15:04:05"))
	var srcLog string
	if instance.UUID != "" {
		srcLog = fmt.Sprintf("EXT [UUID:%s]", instance.UUID)
	} else {
		srcLog = "IN"
	}
	outputString = fmt.Sprintf("%s [%s] [src_log:%s] Goroutine:%d", time.Now().Format("2006-01-02T15:04:05"), level, srcLog, runtime.NumGoroutine())
	if instance.verbose == true {
		cpu, memory := _GetSystemInfo()
		outputString = fmt.Sprintf("%s [CPU:%s%%|RAM:%s]", outputString, fmt.Sprintf("%.2f", cpu), _FormatByte(memory))
	}
	outputString = fmt.Sprintf("%s [%s:%d] [%s] : %s \n", outputString, strings.ToUpper(packageMethodName), line, n.name, message)
	output := []byte(outputString)
	switch level {
	case "INFO", "WARNING", "DEBUG":
		os.Stdout.Write(output)
	case "ERROR":
		os.Stderr.Write(output)
	}
}
func (n *namespace) Info(message string) {
	n._Print("INFO", message)
}
func (n *namespace) Debug(message string) {
	n._Print("DEBUG", message)
}
func (n *namespace) Warning(message string) {
	n._Print("WARNING", message)
}
func (n *namespace) Error(message string) {
	n._Print("ERROR", message)
}
func (n *namespace) Infof(format string, a ...any) {
	n.Info(fmt.Sprintf(format, a...))
}
func (n *namespace) Debugf(format string, a ...any) {
	n.Debug(fmt.Sprintf(format, a...))
}
func (n *namespace) Warningf(format string, a ...any) {
	n.Warning(fmt.Sprintf(format, a...))
}
func (n *namespace) Errorf(format string, a ...any) {
	n.Error(fmt.Sprintf(format, a...))
}
