package logger

import (
	"fmt"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var _LastCpu float64
var _LastMemory int64
var last time.Time
var callSystemInfo bool

func __GetSystemInfo() {
	callSystemInfo = true
	for {
		platform := runtime.GOOS
		if platform != "darwin" && platform != "linux" {
			return
		}
		stdout, _ := exec.Command("ps", "-o", "pcpu,rss", "-p", strconv.Itoa(pid)).Output()
		if len(stdout) == 0 {
			return
		}
		ret := _FormatStdOut(stdout, 1)
		if len(ret) == 0 {
			return
		}
		cpu, err := strconv.ParseFloat(ret[0], 64)
		if err == nil {
			cpu = cpu / float64(runtime.NumCPU())
		}
		memory, err := strconv.ParseInt(ret[1], 10, 64)
		if err == nil {
			memory = memory * 1024
		}
		last = time.Now()

		_LastCpu = cpu
		_LastMemory = memory
		time.Sleep(1 * time.Second)
	}
}
func _GetSystemInfo() (cpu float64, memory int64) {
	if !callSystemInfo {
		go __GetSystemInfo()
	}
	return _LastCpu, _LastMemory
}
func _FormatStdOut(stdout []byte, userfulIndex int) []string {
	infoArr := strings.Split(string(stdout), "\n")[userfulIndex]
	ret := strings.Fields(infoArr)
	return ret
}
func _FormatByte(b int64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f%cO",
		float64(b)/float64(div), "KMGTPE"[exp])
}
func _Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
