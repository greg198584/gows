package logger

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

func init() {
	os.Setenv("ENV", "DEV")
}

func captureOutputStErr(f func()) string {
	rescueStdout := os.Stderr
	r, w, _ := os.Pipe()
	os.Stderr = w
	f()
	w.Close()
	out, _ := ioutil.ReadAll(r)
	os.Stderr = rescueStdout
	return string(out)
}
func captureOutputStOut(f func()) string {
	rescueStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w
	f()
	w.Close()
	out, _ := ioutil.ReadAll(r)
	os.Stdout = rescueStdout
	return string(out)
}
func _Reset() {
	_ = os.Unsetenv("LOG_NAMESPACE")
	_ = os.Unsetenv("LOG_LEVEL")
}
func TestGetLog(t *testing.T) {
	instance := GetLog("TEST")
	assert.NotNil(t, instance)
	assert.IsType(t, instance, new(namespace))
}
func TestGetLogSingleton(t *testing.T) {
	instance := GetLog("TEST")
	assert.NotNil(t, instance)
	assert.IsType(t, instance, new(namespace))
}

func TestNamespace_InfoShowAll(t *testing.T) {
	t.Skip("This test is out of date")
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "*")
	assert.Nil(t, err)
	//Reload()

	output := captureOutputStOut(func() {
		GetLog("TEST").Info("Hello")
		GetLog("TEST2").Info("Hello")
	})
	assert.Contains(t, output, "Hello")
}
func TestNamespace_InfoShowAllNoInfo(t *testing.T) {
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "*")
	assert.Nil(t, err)
	err = os.Setenv("LOG_LEVEL", "debug,warning")
	assert.Nil(t, err)
	//Reload()
	output := captureOutputStOut(func() {
		GetLog("TEST").Info("Hello")
		GetLog("TEST2").Info("Hello")
		GetLog("TEST2").Debug("Hello")
	})
	assert.Contains(t, output, "")
}
func TestNamespace_InfoShowAllWithInfoLevel(t *testing.T) {
	t.Skip("This test is out of date")
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "*")
	assert.Nil(t, err)
	err = os.Setenv("LOG_LEVEL", "debug,warning,info")
	assert.Nil(t, err)
	//Reload()
	output := captureOutputStOut(func() {
		GetLog("TEST").Info("Hello")
		GetLog("TEST2").Info("Hello")
	})
	assert.Contains(t, output, "Hello")
}

//func TestNamespace_InfoHide(t *testing.T) {
//	_Reset()
//	//Reload()
//	output := captureOutputStOut(func() {
//		GetLog("TEST").Info("Hello")
//	})
//	assert.Equal(t, "", output)
//}
//func TestNamespace_DebugLevelHide(t *testing.T) {
//	_Reset()
//	err := os.Setenv("LOG_NAMESPACE", "*")
//	assert.Nil(t, err)
//	err = os.Setenv("LOG_LEVEL", "info")
//	assert.Nil(t, err)
//	//Reload()
//	output := captureOutputStOut(func() {
//		GetLog("TEST").Debug("Hello")
//	})
//	assert.Equal(t, "", output)
//}

func TestNamespace_InfoShow(t *testing.T) {
	t.Skip("This test is out of date")
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "TEST")
	//Reload()
	assert.Nil(t, err)
	output := captureOutputStOut(func() {
		GetLog("TEST").Info("Hello")
	})
	assert.Contains(t, output, "Hello")
}
func TestNamespace_Error(t *testing.T) {
	t.Skip("This test is out of date")
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "TEST")
	//Reload()
	assert.Nil(t, err)
	output := captureOutputStErr(func() {
		GetLog("TEST").Error("Hello")
	})
	assert.Contains(t, output, "Hello")
}
func TestNamespace_Debug(t *testing.T) {
	t.Skip("This test is out of date")
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "TEST")
	//Reload()
	assert.Nil(t, err)
	output := captureOutputStOut(func() {
		GetLog("TEST").Debug("Hello")
	})
	assert.Contains(t, output, "Hello")
}
func TestNamespace_Warning(t *testing.T) {
	t.Skip("This test is out of date")
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "TEST")
	//Reload()
	assert.Nil(t, err)
	output := captureOutputStOut(func() {
		GetLog("TEST").Warning("Hello")
	})
	assert.Contains(t, output, "Hello")
}

func TestNamespace_SubNameSpace(t *testing.T) {
	t.Skip("This test is out of date")
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "TEST.*")
	//Reload()
	assert.Nil(t, err)
	output := captureOutputStOut(func() {
		GetLog("TEST.TOTO").Warning("Hello")
	})
	assert.Contains(t, output, "Hello")
}
func TestNamespace_ManySubNameSpace(t *testing.T) {
	t.Skip("This test is out of date")
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "TEST.*,INFO")
	//Reload()
	assert.Nil(t, err)
	output := captureOutputStOut(func() {
		GetLog("TEST.TOTO").Warning("Hello")
		GetLog("INFO").Warning("Info")
	})
	assert.Contains(t, output, "Hello")
	assert.Contains(t, output, "Info")
}
func TestNamespace_ManySubNameSpaceNew(t *testing.T) {
	t.Skip("This test is out of date")
	_Reset()
	err := os.Setenv("LOG_NAMESPACE", "TEST.*,INFO")
	//Reload()
	assert.Nil(t, err)
	output := captureOutputStOut(func() {
		GetLog("TEST", "TOTO").Warning("Hello")
		GetLog("INFO").Warning("Info")
	})
	assert.Contains(t, output, "Hello")
	assert.Contains(t, output, "Info")
}

//func TestNamespace_ManySubNameSpaceNok(t *testing.T) {
//	_Reset()
//	err := os.Setenv("LOG_NAMESPACE", "TEST.*")
//	//Reload()
//	assert.Nil(t, err)
//	output := captureOutputStOut(func() {
//		GetLog("TEST.TOTO").Warning("Hello")
//		GetLog("TEST", "TOTO").Warning("Hello")
//		GetLog("INFO").Warning("Info")
//	})
//	log.Print(output)
//	assert.Contains(t, output, "Hello")
//	assert.NotContains(t, output, "Info")
//}
//func TestNamespace_SubNameSpaceIncorrect(t *testing.T) {
//	_Reset()
//	err := os.Setenv("LOG_NAMESPACE", "TEST.TATA")
//	//Reload()
//	assert.Nil(t, err)
//	output := captureOutputStOut(func() {
//		GetLog("TEST.TOTO").Warning("Hello")
//	})
//	assert.Equal(t, "", output)
//}
