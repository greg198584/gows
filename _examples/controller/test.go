package controller

import "net/http"

type TestData struct {
	Test1    string      `json:"test1"`
	Context1 interface{} `json:"context1"`
}

type Test struct {
	*MainController
}

func (mc *MainController) Test() *Test {
	return new(Test)
}

func (t *Test) ControllerTest() (err error) {
	t.JSON(http.StatusOK, "API OK")
	return
}
func (t *Test) ControllerTestDB(err error) {
	t.M.GetCnxMaster()
	query := ``
	t.M.Results, err = t.M.Conn.QueryContext(t.Context, query)
	if err != nil {
		t.RetError(http.StatusInternalServerError, err.Error())
	}
	//var datas []Data
	for t.M.Results.Next() {
		//var  data Data
		//t.M.Results.Scan(data.id)
	}
	return
}
func (t *Test) ControllerTestProfilAdmin() (err error) {
	t.JSON(http.StatusOK, t.GetContextData("Jwt"))
	return
}
