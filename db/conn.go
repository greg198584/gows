package db

import (
	"context"
	"database/sql"
)

// Conn : Représente une instance de connexion
type Conn struct {
	config DBParams
	cnx    *sql.Conn
	pool   *sql.DB
}

// QueryContext :
func (c *Conn) QueryContext(ctx context.Context, query string, args ...interface{}) (rows *sql.Rows, err error) {
	var connectionID string
	err = c.cnx.QueryRowContext(ctx, `SELECT CONNECTION_ID()`).Scan(&connectionID)
	if err != nil {
		return nil, err
	}
	rows, err = c.cnx.QueryContext(ctx, query, args...)
	if err != nil {
		if err == context.Canceled || err == context.DeadlineExceeded {
			c.Kill(connectionID)
		}
		return
	}
	return
}

// BeginTx : Ouvre une transaction
func (c *Conn) BeginTx(ctx context.Context, opts *sql.TxOptions) (tx *Tx, err error) {
	var _tx *sql.Tx
	_tx, err = c.cnx.BeginTx(ctx, opts)
	if err != nil {
		return
	}
	var connectionID string
	err = _tx.QueryRowContext(ctx, `SELECT CONNECTION_ID()`).Scan(&connectionID)
	if err != nil {
		return
	}
	tx = new(Tx)
	tx.Tx = _tx
	tx.Conn = c
	tx.ConnectionID = connectionID
	return
}

//QueryRowContext : Exécute une requête qui devrait renvoyer au mieux une ligne
func (c *Conn) QueryRowContext(ctx context.Context, query string, args ...interface{}) *Row {
	var connectionID string
	err := c.cnx.QueryRowContext(ctx, `SELECT CONNECTION_ID()`).Scan(&connectionID)
	if err != nil {
		return nil
	}
	row := new(Row)
	row.c = c
	row.connectionID = connectionID
	row.Row = c.cnx.QueryRowContext(ctx, query, args...)
	return row
}

// ExecContext : Execute une requète SQL
func (c *Conn) ExecContext(ctx context.Context, query string, args ...interface{}) (res sql.Result, err error) {
	var connectionID string
	err = c.cnx.QueryRowContext(ctx, `SELECT CONNECTION_ID()`).Scan(&connectionID)
	if err != nil {
		return
	}
	res, err = c.cnx.ExecContext(ctx, query, args...)
	if err != nil {
		if err == context.Canceled || err == context.DeadlineExceeded {
			c.Kill(connectionID)
		}
		return
	}
	return
}

// ExecContextLastInsertId : Execute une requète SQL et récupère le dernier ID inséré
func (c *Conn) ExecContextLastInsertId(ctx context.Context, query string, args ...interface{}) (id int64, err error) {
	var res sql.Result
	res, err = c.ExecContext(ctx, query, args...)
	if err == nil {
		id, err = res.LastInsertId()
	}

	return
}

// ExecContextRowsAffected : Execute une requète SQL et récupère le nombre de ligne affecté
func (c *Conn) ExecContextRowsAffected(ctx context.Context, query string, args ...interface{}) (id int64, err error) {
	var res sql.Result
	res, err = c.ExecContext(ctx, query, args...)
	if err == nil {
		id, err = res.RowsAffected()
	}

	return
}

// QueryValueContext : Execute une requète SQL de type SELECT et récupère la première valeur de la première ligne
func QueryValueContext[T any](c *Conn, ctx context.Context, query string, args ...any) (columnValue T, err error) {
	err = c.QueryRowContext(ctx, query, args...).Scan(&columnValue)

	return
}

// Close : Renvoie la connexion sql au pool de connexions
func (c *Conn) Close() error {
	return c.cnx.Close()
}

// Kill : Permet de fermer une connexion mysql
func (c *Conn) Kill(connectionID string) {
	_, _ = c.pool.Exec("KILL ?", connectionID)
}
