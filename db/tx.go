package db

import (
	"context"
	"database/sql"
)

type Tx struct {
	Conn         *Conn
	Tx           *sql.Tx
	ConnectionID string
	Row          *sql.Row
}

func (t *Tx) QueryContext(ctx context.Context, query string, args ...interface{}) (rows *sql.Rows, err error) {
	rows, err = t.Tx.QueryContext(ctx, query, args...)
	if err != nil {
		if err == context.Canceled || err == context.DeadlineExceeded {
			t.Conn.Kill(t.ConnectionID)
		}
		_ = t.Tx.Rollback()
		return
	}
	return
}
func (t *Tx) QueryRowContext(ctx context.Context, query string, args ...interface{}) *Row {
	row := new(Row)
	row.c = t.Conn
	row.connectionID = t.ConnectionID
	row.Tx = t.Tx
	row.Row = t.Tx.QueryRowContext(ctx, query, args...)
	return row
}
func (t *Tx) ExecContext(ctx context.Context, query string, args ...interface{}) (res sql.Result, err error) {
	res, err = t.Tx.ExecContext(ctx, query, args...)
	if err != nil {
		if err == context.Canceled || err == context.DeadlineExceeded {
			t.Conn.Kill(t.ConnectionID)
		}
		_ = t.Tx.Rollback()
		return
	}
	return
}
func (t *Tx) Commit() error {
	return t.Tx.Commit()
}
func (t *Tx) Rollback() error {
	return t.Tx.Rollback()
}
