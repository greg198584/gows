package db

import (
	"context"
	"database/sql"
)

type Row struct {
	c            *Conn
	connectionID string
	Tx           *sql.Tx
	Row          *sql.Row
}

// Scan : Surcharge de la methode scan du package sql
func (r *Row) Scan(dest ...interface{}) error {
	err := r.Row.Scan(dest...)
	if err != nil {
		if err == context.Canceled || err == context.DeadlineExceeded {
			r.c.Kill(r.connectionID)
			if r.Tx != nil {
				err = r.Tx.Rollback()
			}
		}
		return err
	}
	return err
}
