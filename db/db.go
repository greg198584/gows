package db

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/greg198584/gows/config"
	"gitlab.com/greg198584/gows/constant"
	"gitlab.com/greg198584/gows/logger"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

const NAMESPACE = "mysql"

var connectorList = sync.Map{}
var lock = &sync.Mutex{}

type ConnectorConfig struct {
	DefaultConnector    default_connector      `json:"default_connector"`
	AdditionalConnector map[string]interface{} `json:"additional_connector"`
}

type default_connector struct {
	MasterParams DBParams `json:"master"`
	SlaveParams  DBParams `json:"slave"`
	DevParams    DBParams `json:"dev"`
}

type DBParams struct {
	Name     string `json:"name"`
	Driver   string `json:"driver"`
	User     string `json:"user"`
	Password string `json:"password"`
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Database string `json:"database"`
	Locale   string `json:"locale"`
}

type Connectors struct {
	Master Connector
	Slave  Connector
}

// Connector : Represente un connector
type Connector struct {
	name   string
	config DBParams
	sql    *sql.DB
	ctx    context.Context
}

func Init(ctx context.Context) (Connectors Connectors, err error) {
	var DbConfig ConnectorConfig
	err = config.Get("db", &DbConfig)
	if err != nil {
		return
	}
	if os.Getenv("ENV") == "DEV" {
		var ConnectorMaster Connector
		ConnectorMaster, err = Get(ctx, constant.DB_DEV, DbConfig.DefaultConnector.DevParams)
		if err != nil {
			return
		}
		Connectors.Master = ConnectorMaster
		Connectors.Slave = ConnectorMaster
	} else {
		Connectors.Master, err = Get(ctx, constant.DB_MASTER, DbConfig.DefaultConnector.MasterParams)
		if err != nil {
			return
		}
		Connectors.Slave, err = Get(ctx, constant.DB_SLAVE, DbConfig.DefaultConnector.SlaveParams)
		if err != nil {
			return
		}
	}
	return
}
func Get(ctx context.Context, connectorName string, config DBParams) (sql Connector, err error) {
	lock.Lock()
	defer lock.Unlock()
	cnx, ok := connectorList.Load(connectorName)
	if ok {
		sql = cnx.(Connector)
		sql.ctx = ctx
		return
	}
	var p *Connector
	p, err = _NewConnector(connectorName, config)
	if err != nil {
		logger.GetLog(NAMESPACE).Error(err.Error())
		return
	}
	sql = *p
	connectorList.Store(connectorName, *p)
	sql.ctx = ctx
	return
}
func _NewConnector(name string, config DBParams) (p *Connector, err error) {
	p = new(Connector)
	p.name = name
	p.config = config
	err = p._Create()
	if err != nil {
		logger.GetLog(NAMESPACE).Error(err.Error())
		return
	}
	return
}
func (c *Connector) _NameSpace() string {
	return fmt.Sprintf("%s.%s", NAMESPACE, c.name)
}
func (c *Connector) _Create() (err error) {
	logger.GetLog(c._NameSpace()).Debug("Status=Connecting")
	dataSource := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true&loc=%s", c.config.User, c.config.Password, c.config.Host, c.config.Port, c.config.Database, c.config.Locale)
	c.sql, err = sql.Open("mysql", dataSource)
	if err != nil {
		logger.GetLog(c._NameSpace()).Error(err.Error())
		return
	}
	go func() {
		done := make(chan os.Signal, 1)
		signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
		<-done
		logger.GetLog(c._NameSpace(), "SIGNAL").Debug("Closing connector, We wait 2sec for the context cancellation!")
		time.Sleep(2 * time.Second)
		err = c.sql.Close()
		if err != nil {
			logger.GetLog(c._NameSpace()).Error(err.Error())
			return
		}
	}()
	err = c.sql.Ping()
	if err != nil {
		logger.GetLog(c._NameSpace()).Error(err.Error())
		return
	}
	c.sql.SetConnMaxLifetime(time.Minute * 3)
	c.sql.SetMaxOpenConns(10)
	c.sql.SetMaxIdleConns(10)
	logger.GetLog(c._NameSpace()).Debug("Status=Connected")
	return
}

func (c *Connector) GetCnx() (*Conn, error) {
	if c.sql == nil {
		return nil, errors.New(fmt.Sprintf("the `%s` connector is not initialized", c.name))
	}
	cnx, err := c.sql.Conn(c.ctx)
	if err != nil {
		logger.GetLog(c._NameSpace()).Error(err.Error())
		return nil, err
	}
	con := new(Conn)
	con.pool = c.sql
	con.cnx = cnx
	con.config = c.config
	return con, err
}
