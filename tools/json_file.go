package tools

import (
	"encoding/json"
	"io/ioutil"
)

func CreateJsonFile(filepath string, data any) (err error) {
	file, err := json.MarshalIndent(data, "", " ")
	if err != nil {
		return
	}
	err = ioutil.WriteFile(filepath, file, 0644)
	return
}
