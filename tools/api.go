package tools

import (
	"bytes"
	"encoding/json"
	"errors"
	"gitlab.com/greg198584/gows/logger"
	"io/ioutil"
	"net/http"
	"time"
)

type Api struct {
	Url        string
	Method     string
	Token      string
	AuthConfig Auth
	PostData   []byte
	Timeout    time.Duration
}

type Auth struct {
	Url    string
	Config AuthConfig
}

type AuthConfig struct {
	Login    string   `json:"login"`
	Password string   `json:"password"`
	Customer string   `json:"customer"`
	Scopes   []string `json:"scopes"`
}

type AuthData struct {
	ID        int64  `json:"Id"`
	Nom       string `json:"Nom"`
	Prenom    string `json:"Prenom"`
	Login     string `json:"Login"`
	Token     Token  `json:"Token"`
	Iat       int64  `json:"Iat"`
	TokenType string `json:"TokenType"`
}

type Token struct {
	Token string `json:"Token"`
}

func GetApi(method string, url string, authConf Auth, timeout int) *Api {
	logger.GetLog("API").Debug("GET API")
	var api Api
	api.Url = url
	api.Method = method
	api.AuthConfig = authConf
	api.Timeout = time.Duration(timeout)
	return &api
}

func (a *Api) getToken() (token string, err error) {
	url := a.AuthConfig.Url
	jsonValue, _ := json.Marshal(a.AuthConfig.Config)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonValue))
	if err != nil {
		logger.GetLog("API").Error(err.Error())
		return token, err
	}
	req.Header.Set("Content-Type", "application/json")

	cli := &http.Client{}
	resp, err := cli.Do(req)

	if err != nil {
		logger.GetLog("API").Error(err.Error())
		return token, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer cli.CloseIdleConnections()
	if err != nil {
		logger.GetLog("API").Error(err.Error())
		return token, err
	}
	if resp.StatusCode != 200 {
		err = errors.New(string(body))
		logger.GetLog("API").Error(err.Error())
		return token, err
	}
	var result AuthData
	err = json.Unmarshal(body, &result)
	if err != nil {
		logger.GetLog("API").Error(err.Error())
		return token, err
	}
	token = result.Token.Token
	return token, err
}

func (a *Api) IsAuthenticated() (authenticated bool, err error) {
	// On verifie si on a deja un token sinon appel api pour authenficition
	if a.Token != "" {
		return true, err
	} else {
		token, err := a.getToken()
		if err != nil {
			logger.GetLog("API").Error(err.Error())
			return false, err
		}
		isAuthenticated := false
		if token != "" {
			isAuthenticated = true
			a.Token = token
		}
		return isAuthenticated, err
	}
}

func (a *Api) Exec(data []byte) (result []byte, retStatusCode int, err error) {
	is_auth, err := a.IsAuthenticated()
	if is_auth {
		req, err := http.NewRequest(a.Method, a.Url, bytes.NewBuffer(data))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Add("authorization", "Bearer "+a.Token)

		cli := &http.Client{
			Timeout: a.Timeout * time.Minute,
		}
		resp, err := cli.Do(req)
		retStatusCode = resp.StatusCode
		if err != nil {
			logger.GetLog("API").Error(err.Error())
			return result, retStatusCode, err
		}
		result, err = ioutil.ReadAll(resp.Body)
		defer cli.CloseIdleConnections()
	}
	return result, retStatusCode, err
}
