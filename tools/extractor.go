package tools

import (
	"regexp"
	"strconv"
	"strings"
)

type extractor struct {
	focused     string
	focusedList []string
	key         *int64
}

func GetExtractor(data interface{}, focused string) interface{} {
	e := new(extractor)
	e.focused = focused
	e._Analyse()

	// Dispatch search
	switch d := data.(type) {
	case map[string]interface{}:
		return e._ExtractObjectValue(d)
	case []interface{}:
		return e._ExtractArrayValue(d)
	default:
		return nil
	}
}
func (e *extractor) _Analyse() {
	e.focusedList = strings.Split(strings.TrimSpace(e.focused), ".")
	e.focused = e.focusedList[0]
	var re = regexp.MustCompile(`(?m)[\d]`)
	var tab = re.FindAllString(e.focusedList[0], -1)
	if len(tab) > 0 {
		n, err := strconv.ParseInt(tab[0], 10, 64)
		if err == nil {
			e.key = &n
			e.focused = strings.Replace(e.focusedList[0], "["+tab[0]+"]", "", -1)
		}
	}
}
func (e *extractor) _ExtractObjectValue(data map[string]interface{}) interface{} {
	if val, ok := data[e.focused]; ok {
		if len(e.focusedList) > 1 {
			var next map[string]interface{}
			if e.key == nil {
				next = data[e.focused].(map[string]interface{})
			} else {
				next = data[e.focused].([]interface{})[*e.key].(map[string]interface{})
			}
			return GetExtractor(next, strings.Join(e.focusedList[1:], "."))
		}
		return val
	}
	return nil
}
func (e *extractor) _ExtractArrayValue(data []interface{}) interface{} {
	if e.key == nil {
		return nil
	}
	return GetExtractor(data[*e.key], strings.Join(e.focusedList[1:], "."))
}
