package tools

import (
	"fmt"
	"strconv"
)

func IsInt(value string) bool {
	_, err := strconv.Atoi(value)
	if err != nil {
		return false
	}
	return true
}
func ToInt(value interface{}) *int64 {
	var n int64
	var err error
	switch t := value.(type) {
	case float64:
		s := fmt.Sprintf("%.0f", t)
		n, err = strconv.ParseInt(s, 10, 64)
		break
	case int:
		n = int64(t)
		break
	case string:
		n, err = strconv.ParseInt(t, 10, 64)
		break
	default:
		return nil
	}
	if err != nil {
		return nil
	}
	return &n
}
