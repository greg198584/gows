package tools

import (
	"crypto/sha1"
	"encoding/hex"
	"golang.org/x/crypto/bcrypt"
)

func GenerateHash(data []byte) (hash string) {
	hasher := sha1.New()
	hasher.Write(data)
	hash = hex.EncodeToString(hasher.Sum(nil))
	return
}

func GenerateHashSalt(data []byte) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword(data, 14)
	return string(bytes), err
}

func CompareHashSalt(data []byte, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), data)
	return err == nil
}
