package tools

import (
	"bytes"
	"encoding/json"
	"fmt"
)

func _prettyString(str []byte) (string, error) {
	var prettyJSON bytes.Buffer
	if err := json.Indent(&prettyJSON, str, "", "    "); err != nil {
		return "", err
	}
	return prettyJSON.String(), nil
}

func DumpJson(data interface{}) {
	jsonPretty, _ := _prettyString([]byte(data.(string)))
	fmt.Println(jsonPretty)
}
