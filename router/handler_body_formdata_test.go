package router

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"testing"
)

func (dc *DefaultController) BodyMultiPartForm() (err error) {
	err = dc.RequireBody(BODY_MUTLIPART_FORM)
	if err != nil {
		return
	}
	value := dc.GetFormValue("NOK")
	if value == "" {
		//
	}
	if dc.GetMultiFormFormFile("test") == nil {
		dc.JSON(http.StatusOK, R{"uuid": dc.GetUUID(), "message": dc.GetMultiFormFormValue("message"), "file": nil})
		return
	}
	dc.JSON(http.StatusOK, R{"uuid": dc.GetUUID(), "query": dc.GetMultiFormFormValue("test"), "file": dc.GetMultiFormFormFile("test").Filename})
	return
}

func TestBodyFormDataEmpty(t *testing.T) {
	router := _Load(t)
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping", nil)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", "multipart/form-data")
	handler := http.Handler(NewHandler(router.routes[6], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	assert.Contains(t, string(body), "API_BODY_FORM_DATA_EMPTY")
}
func TestBodyFormDataValid(t *testing.T) {
	router := _Load(t)

	post := &bytes.Buffer{}
	writer := multipart.NewWriter(post)
	_ = writer.WriteField("message", "test")
	_ = writer.Close()

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping", post)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())
	handler := http.Handler(NewHandler(router.routes[6], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	assert.Contains(t, string(body), "uuid")
}
func TestBodyFormDataValidWihoutContentType(t *testing.T) {
	router := _Load(t)

	post := &bytes.Buffer{}
	writer := multipart.NewWriter(post)
	_ = writer.WriteField("message", "test")
	_ = writer.Close()

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping", post)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", "application/json")
	handler := http.Handler(NewHandler(router.routes[6], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	assert.Contains(t, string(body), "API_BODY_TYPE_FAIL")
}
