package router

import (
	"context"
	"fmt"
	"gitlab.com/greg198584/gows/logger"
	"time"
)

type Middleware struct {
}

func (m *Middleware) Helmet() func(h Handler) Handler {
	return func(h Handler) Handler {
		return HandlerFunc(func(c *Context) (err error) {
			c.SetHeader("X-Content-Type-Options", "nosniff")
			c.SetHeader("X-Content-Type-Options", "nosniff")
			c.SetHeader("x-dns-prefetch-control", "off")
			c.SetHeader("x-download-options", "noopen")
			c.SetHeader("x-frame-options", "DENY")
			c.SetHeader("x-xss-protection", "1; mode=block")
			c.SetHeader("strict-transport-security", "max-age=15552000; includeSubDomains")
			return h.Next(c)
		})
	}
}

func (m *Middleware) TimeOut(data struct {
	Value time.Duration `json:"value"`
}) func(h Handler) Handler {
	return func(h Handler) Handler {
		return HandlerFunc(func(c *Context) (err error) {
			now := time.Now()
			var cancel context.CancelFunc
			c.Context, cancel = context.WithTimeout(c.Context, data.Value*time.Second)
			defer cancel()
			err = h.Next(c)
			if c.Context.Err() == context.DeadlineExceeded {
				err = HTTPRequestTimeout().SetCode(API_EXCEEDED_TIMEOUT).SetMessage(fmt.Sprintf("the request to exceed the maximum execution time (%ds)", data.Value))
				return
			}
			if c.Context.Err() == context.Canceled {
				after := time.Now().Sub(now)
				logger.GetLog("router", "middleware", "timeout").Warning(fmt.Sprintf("%s abord request to %s after %s", c.GetRemoteIP(), c.GetRemoteURI(), after))
			}
			return
		})
	}
}
func (m *Middleware) ResponseTime(data struct {
	Render string `json:"render"`
}) func(h Handler) Handler {
	return func(h Handler) Handler {
		return HandlerFunc(func(c *Context) (err error) {
			now := time.Now()
			err = h.Next(c)
			if err != nil {
				return
			}
			after := time.Now().Sub(now)

			var value string
			switch data.Render {
			case "ms":
				value = fmt.Sprintf("%.2fms", float64(after.Nanoseconds()/1000000))
			default:
				value = after.String()
			}
			c.SetHeader("x-response-time", value)
			return
		})
	}
}
