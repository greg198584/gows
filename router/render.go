package router

import (
	"context"
	"encoding/json"
	"fmt"
	sentry "github.com/getsentry/sentry-go"
	"gitlab.com/greg198584/gows/config"
	"gitlab.com/greg198584/gows/logger"
	"gitlab.com/greg198584/gows/promise"
	"net/http"
	"os"
	"reflect"
	"strings"
	"time"
)

//noinspection ALL
const (
	ENV_PRODUCTION           = "PROD"
	API_INTERNAL_ERROR       = "API_INTERNAL_ERROR"
	API_INVOKE_ERROR         = "API_INVOKE_ERROR"
	API_BODY_JSON_PARSE_FAIL = "API_BODY_JSON_PARSE_FAIL"
	API_BODY_XML_PARSE_FAIL  = "API_BODY_XML_PARSE_FAIL"
	API_BODY_EOF             = "API_BODY_EOF"
	API_BODY_TOO_LARGE       = "API_BODY_TOO_LARGE"
	API_BODY_FORM_EMPTY      = "API_BODY_FORM_EMPTY"
	API_BODY_FORM_DATA_EMPTY = "API_BODY_FORM_DATA_EMPTY"
	API_BODY_JSON_TYPE_FAIL  = "API_BODY_JSON_TYPE_FAIL"
	API_BODY_XML_TYPE_FAIL   = "API_BODY_XML_TYPE_FAIL"
	API_BODY_TYPE_FAIL       = "API_BODY_TYPE_FAIL"
	API_BODY_VALIDATOR_FAIL  = "API_BODY_VALIDATOR_FAIL"
	API_QUERY_VALIDATOR_FAIL = "API_QUERY_VALIDATOR_FAIL"
	API_FORM_VALIDATOR_FAIL  = "API_FORM_VALIDATOR_FAIL"
	API_EXCEEDED_TIMEOUT     = "API_EXCEEDED_TIMEOUT"
)

type JSONRender interface {
	SetError(err error)
	SetContext(c *Context)
	SetResponseWriter(w http.ResponseWriter) *promise.Promise
	SetRequest(request *http.Request)
	GetContext() *Context
	Send() *promise.Promise
}

type Render struct {
	err    error
	c      *Context
	w      http.ResponseWriter
	r      *http.Request
	custom *JSONRender
}

func (r *Render) SetResponseWriter(w http.ResponseWriter) *promise.Promise {
	r.w = w
	return promise.Resolve(r.c, r)
}
func (r *Render) SetRequest(request *http.Request) {
	r.r = request
}
func (r *Render) SetError(err error) {
	r.err = err
}
func (r *Render) SetContext(c *Context) {
	r.c = c
}
func (r *Render) GetContext() *Context {
	return r.c
}

func (r *Render) Send() *promise.Promise {
	if r.r.Context().Err() == context.Canceled {
		return promise.Resolve(r.c.Context, r)
	}
	r._CatchError()
	r._Respond()
	return promise.Resolve(r.c.Context, r)
}
func (r *Render) _CatchError() {
	env := strings.ToUpper(os.Getenv("ENV"))
	if env == "" {
		env = ENV_PRODUCTION
	}
	if r.err != nil {
		IsSentryActive := config.GetValue("http", "sentry").(bool)
		sentryDSN := config.GetValue("http", "sentry_dsn").(string)
		if IsSentryActive {
			sentry.Init(sentry.ClientOptions{
				Dsn: sentryDSN,
				// Set TracesSampleRate to 1.0 to capture 100%
				// of transactions for performance monitoring.
				// We recommend adjusting this value in production,
				TracesSampleRate: 1.0,
			})
			// Flush buffered events before the program terminates.
			defer sentry.Flush(2 * time.Second)
			sentry.CaptureException(r.err)
		}
		switch t := r.err.(type) {
		case HTTPGenericError:
			details := t.Details()
			if details == nil {
				details = make(map[string]interface{})
			}
			logger.GetLog("Render").Debug(fmt.Sprintf("%v", R{"Code": t.Code(), "Uuid": r.c.GetUUID(), "Message": t.Message(), "Details": details, "Trace": t.Trace()}))
			if env == ENV_PRODUCTION {
				if t.OnDev() {
					//apm.CaptureError(r.c.Context, r.err).Send()
					r.c.JSON(t.StatusCode(), R{"Code": API_INTERNAL_ERROR, "Uuid": r.c.GetUUID(), "Message": "An error has occurred"})
					break
				}
				r.c.JSON(t.StatusCode(), R{"Code": t.Code(), "Uuid": r.c.GetUUID(), "Message": t.Message(), "Details": details})
				break
			}
			r.c.JSON(t.StatusCode(), R{"Code": t.Code(), "Uuid": r.c.GetUUID(), "Message": t.Message(), "Details": details, "Trace": t.Trace()})
			break
		case ErrorStackTrace:
			//apm.CaptureError(r.c.Context, r.err).Send()
			logger.GetLog("Render").Debug(fmt.Sprintf("%v", R{"Code": API_INTERNAL_ERROR, "Uuid": r.c.GetUUID(), "Message": t.Error(), "Trace": t.Trace()}))
			if env == ENV_PRODUCTION {
				r.c.JSON(http.StatusInternalServerError, R{"Code": API_INTERNAL_ERROR, "Uuid": r.c.GetUUID(), "Message": "An error has occurred"})
				break
			}
			r.c.JSON(http.StatusInternalServerError, R{"Code": API_INTERNAL_ERROR, "Uuid": r.c.GetUUID(), "Message": t.Error(), "Trace": t.Trace()})
			break
		default:
			//apm.CaptureError(r.c.Context, r.err).Send()
			logger.GetLog("Render").Debug(fmt.Sprintf("%v", R{"Code": API_INTERNAL_ERROR, "Uuid": r.c.GetUUID(), "Type": reflect.TypeOf(r.err).String(), "Message": r.err.Error(), "Details": make(map[string]interface{})}))
			if env == ENV_PRODUCTION {
				r.c.JSON(http.StatusInternalServerError, R{"Code": API_INTERNAL_ERROR, "Uuid": r.c.GetUUID(), "Message": "An error has occurred"})
				break
			}
			r.c.JSON(http.StatusInternalServerError, R{"Code": API_INTERNAL_ERROR, "Uuid": r.c.GetUUID(), "Type": reflect.TypeOf(r.err).String(), "Message": r.err.Error(), "Details": make(map[string]interface{})})
		}
	}
}
func (r *Render) _Respond() {
	//span, _ := apm.StartSpan(r.r.Context(), "http", "response")
	//go func() {
	//	<-r.r.Context().Done()
	//	span.End()
	//}()
	r.w.Header().Add("Content-Type", "application/json")
	r.w.WriteHeader(r.c.StatusCode())
	if r.c.Response() != nil {
		err := json.NewEncoder(r.w).Encode(r.c.Response())
		if err != nil {
			r.w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}
func NewRender(ctx *Context, custom JSONRender) *promise.Promise {
	if reflect.TypeOf(custom) == nil {
		var j JSONRender
		custom = j
		custom = &Render{}
	}
	custom.SetContext(ctx)
	return promise.Resolve(ctx.Context, custom)
}
