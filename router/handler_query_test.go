package router

import (
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSimpleFailInvoke(t *testing.T) {
	router := _Load(t)
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping/1", nil)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req = mux.SetURLVars(req, map[string]string{
		"id": "1",
	})

	handler := http.Handler(NewHandler(router.routes[2], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}
func TestSimpleQuery(t *testing.T) {
	router := _Load(t)
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping/1", nil)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req = mux.SetURLVars(req, map[string]string{
		"id": "1",
	})

	handler := http.Handler(NewHandler(router.routes[2], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//func TestSimpleQueryPayload(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/ping/1?date=2018", nil)
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//
//	req = mux.SetURLVars(req, map[string]string{
//		"id": "1",
//	})
//
//	handler := http.Handler(NewHandler(router.routes[9], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusOK {
//		t.Errorf("handler returned wrong status code: got %v want %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), "{\"id\":1,\"params\":{\"date\":[\"2018\"]}}")
//}
//func TestSimpleBadQueryPayload(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/ping/toto", nil)
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//
//	req = mux.SetURLVars(req, map[string]string{
//		"id": "toto",
//	})
//
//	handler := http.Handler(NewHandler(router.routes[9], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want %v",
//			status, http.StatusBadRequest)
//	}
//
//	assert.Contains(t, string(body), "API_QUERY_VALIDATOR_FAIL")
//}
//func TestSimpleBadQuery(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/ping/1", nil)
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//
//	req = mux.SetURLVars(req, map[string]string{
//		"id": "hello ",
//	})
//
//	handler := http.Handler(NewHandler(router.routes[2], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want %v",
//			status, http.StatusBadRequest)
//	}
//
//	assert.Contains(t, string(body), "API_QUERY_VALIDATOR_FAIL")
//}
