package router

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"net/http"
	"os"
	"testing"
)

type DefaultController struct {
	*ApiController
}

type AppMiddleware struct {
}

func (am *AppMiddleware) Helmet() func(h Handler) Handler {
	return func(h Handler) Handler {
		return HandlerFunc(func(c *Context) (err error) {
			return h.Next(c)
		})
	}
}

type AppMiddlewarePanic struct {
}

func (am *AppMiddlewarePanic) Helmet() func(h Handler) Handler {
	return func(h Handler) Handler {
		return HandlerFunc(func(c *Context) (err error) {
			panic("err")
			// return h.Next(c)
		})
	}
}

type DefaultControllerEmpty struct {
	*ApiController
}
type ApiController struct {
	*Context
}

var GeneratePanic = false

func (ac *ApiController) OnCreate() (err error) {
	if GeneratePanic {
		panic("panic")
	}
	return
}
func (ac *ApiController) OnFinish() (err error) {
	return
}

func init() {
	_ = os.Setenv("APP_PATH", "../dev")
}
func (c *ApiController) DefaultControllerFail() {
}
func (c *ApiController) DefaultControllerEmpty() *DefaultControllerEmpty {
	return new(DefaultControllerEmpty)
}
func (c *ApiController) DefaultController() *DefaultController {
	return new(DefaultController)
}
func (dc *DefaultController) Ping() (err error) {
	dc.JSON(http.StatusOK, R{"message": "Hello World"})
	return
}
func (dc *DefaultController) PingPayload(payload struct {
	Id *int64 `type:"query"`
}) (err error) {
	dc.JSON(http.StatusOK, R{"id": payload.Id, "params": dc.GetQueryParams()})
	return
}
func (dc *DefaultController) PingBadReturn() {
	dc.JSON(http.StatusOK, R{"message": "Hello World"})
	return
}
func (dc *DefaultController) PingBadInput(c *Context) {
	dc.JSON(http.StatusOK, R{"message": "Hello World"})
	return
}
func (dc *DefaultController) BodyJSON() (err error) {
	type Body struct {
		Code    string `json:"code" validate:"required"`
		Message []struct {
			Code int `json:"code" validate:"required"`
		} `json:"message" validate:"gte=1,required,dive,required"`
	}

	var body Body
	//err = dc.ValidateBody(&body)
	if err != nil {
		return
	}

	dc.JSON(http.StatusOK, R{"body": body, "info": dc.GetHeader()})
	return
}
func (dc *DefaultController) BodyJSONRequireMethod1() (err error) {
	err = dc.RequireBody(BODY_JSON)
	if err != nil {
		return
	}
	type Body struct {
		Code    string `json:"code" validate:"required"`
		Bool    *bool  `json:"bool" validate:"required"`
		Message []struct {
			Code int `json:"code" validate:"required"`
		} `json:"message" validate:"gte=1,required,dive,required"`
	}

	var body Body
	//err = dc.ValidateBody(&body)
	if err != nil {
		return
	}

	dc.JSON(http.StatusOK, R{"body": body, "header": dc.GetHeader()})
	return
}
func (dc *DefaultController) BodyJSONRequireMethod2(payload struct {
	Body struct {
		Code    string `json:"code" validate:"required"`
		Message []struct {
			Code int `json:"code" validate:"required"`
		} `json:"message" validate:"gte=1,required,dive,required"`
	} `type:"json"`
}) (err error) {

	dc.JSON(http.StatusOK, R{"body": payload})
	return
}

//func (dc *DefaultController) BodyFile() (err error) {
//	err = dc.RequireBody(BODY_FILE)
//	if err != nil {
//		return
//	}
//	dir, err := ioutil.TempDir("", "unit_test")
//	if err != nil {
//		return
//	}
//	err = dc.GetFile().Save(fmt.Sprintf("%s/%s", dir, dc.GetUUID()))
//	if err != nil {
//		return
//	}
//	err = os.Remove(fmt.Sprintf("%s/%s", dir, dc.GetUUID()))
//	if err != nil {
//		return
//	}
//	dc.JSON(http.StatusOK, R{"uuid": dc.GetUUID(), "content-type": dc.GetFile().ContentType(), "data": dc.GetFile().Get()})
//	return
//}
//func (dc *DefaultController) BodyFileProto(body struct {
//	File *File `type:"file"`
//}) (err error) {
//	dir, err := ioutil.TempDir("", "unit_test")
//	if err != nil {
//		return
//	}
//	err = body.File.Save(fmt.Sprintf("%s/%s", dir, dc.GetUUID()))
//	if err != nil {
//		return
//	}
//	err = os.Remove(fmt.Sprintf("%s/%s", dir, dc.GetUUID()))
//	if err != nil {
//		return
//	}
//	dc.JSON(http.StatusOK, R{"uuid": dc.GetUUID(), "content-type": dc.GetFile().ContentType(), "data": dc.GetFile().Get()})
//	return
//}

func PrintJsonResponse(t *testing.T, b []byte) {
	var out bytes.Buffer
	err := json.Indent(&out, b, "", "  ")
	if err != nil {
		return
	}

	t.Logf("\nResponse JSON :\n %s", out.Bytes())
}

func _Load(t *testing.T) *router {
	router := New(&ApiController{})
	err := router._Load()
	if !assert.Nil(t, err) {
		return nil
	}

	assert.Nil(t, router.Error())
	assert.NotNil(t, router)

	assert.NotZero(t, len(router.payload))
	assert.Equal(t, router.routes[0].method, "POST")
	assert.Equal(t, router.routes[0].pattern, "/api/ping")

	return router
}

//func TestErrorOnCreate(t *testing.T) {
//	GeneratePanic = true
//	defer func() {
//		GeneratePanic = false
//	}()
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/ping/:id", nil)
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//
//	req = mux.SetURLVars(req, map[string]string{
//		"id": "1",
//	})
//	handler := http.Handler(NewHandler(router.routes[0], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusInternalServerError {
//		t.Errorf("handler returned wrong status code: got %v want %v",
//			status, http.StatusOK)
//	}
//}
//func TestSimpleHandler(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/ping/:id", nil)
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//
//	req = mux.SetURLVars(req, map[string]string{
//		"id": "1",
//	})
//	handler := http.Handler(NewHandler(router.routes[0], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusOK {
//		t.Errorf("handler returned wrong status code: got %v want %v",
//			status, http.StatusOK)
//	}
//}
//func TestBodyJsonHandlerEmptyWithContentTypeBis(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json", bytes.NewBuffer([]byte("")))
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	req.Header.Set("Content-Type", "application/json")
//	handler := http.Handler(NewHandler(router.routes[11], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), "API_BODY_EOF")
//}
//func TestBodyJsonHandlerEmptyBis(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json", bytes.NewBuffer([]byte("")))
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	// req.Header.Set("Content-Type", "application/json")
//	handler := http.Handler(NewHandler(router.routes[11], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), "API_BODY_TYPE_FAIL")
//}
//func TestBodyJsonHandlerNilWithContentTypeBis(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json", nil)
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	req.Header.Set("Content-Type", "application/json")
//	handler := http.Handler(NewHandler(router.routes[11], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), API_BODY_EOF)
//}
//func TestBodyJsonHandlerEmpty(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json", bytes.NewBuffer([]byte("")))
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	// req.Header.Set("Content-Type", "application/json")
//	handler := http.Handler(NewHandler(router.routes[4], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), "API_BODY_EOF")
//}
//
//func TestBodyJsonHandlerNilWithContentType(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json", nil)
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	req.Header.Set("Content-Type", "application/json")
//	handler := http.Handler(NewHandler(router.routes[4], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), API_BODY_EOF)
//}
//func TestBodyJsonHandlerEmptyRequire(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json/require", bytes.NewBuffer([]byte("")))
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	req.Header.Set("Content-Type", "application/json")
//	handler := http.Handler(NewHandler(router.routes[3], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), "API_BODY_EOF")
//}
//func TestBodyJsonHandlerJsonNotValid(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json", bytes.NewBuffer([]byte(
//		"{",
//	)))
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	req.Header.Set("Content-Type", "application/json")
//	handler := http.Handler(NewHandler(router.routes[3], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), "API_BODY_JSON_PARSE_FAIL")
//}
//func TestBodyJsonHandlerBadContentType(t *testing.T) {
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json", nil)
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	req.Header.Set("Content-Type", "application/xml")
//	handler := http.Handler(NewHandler(router.routes[3], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//
//	assert.Contains(t, string(body), "API_BODY_TYPE_FAIL")
//}
//func TestBodyJsonHandlerNotEmpty(t *testing.T) {
//	message := map[string]interface{}{
//		"code": "world",
//		"bool": true,
//		"message": []map[string]interface{}{{
//			"code": 1,
//		}},
//	}
//
//	bytesRepresentation, err := json.Marshal(message)
//	if err != nil {
//		t.Fatalf(err.Error())
//	}
//
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json", bytes.NewBuffer(bytesRepresentation))
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	req.Header.Set("Content-Type", "application/json")
//	handler := http.Handler(NewHandler(router.routes[3], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusOK {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), "application/json")
//}
//func TestBodyJsonHandlerFieldRequired(t *testing.T) {
//	message := map[string]interface{}{
//		"code": "",
//		"message": []map[string]interface{}{{
//			"code": 1,
//		}},
//	}
//
//	bytesRepresentation, err := json.Marshal(message)
//	if err != nil {
//		t.Fatalf(err.Error())
//	}
//
//	router := _Load(t)
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/json", bytes.NewBuffer(bytesRepresentation))
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//	req.Header.Set("Content-Type", "application/json")
//	handler := http.Handler(NewHandler(router.routes[3], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want: %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), "API_BODY_VALIDATOR_FAIL")
//	assert.Contains(t, string(body), `{"field":"code","need":"required","value":""}`)
//}
