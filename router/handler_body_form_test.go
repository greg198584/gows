package router

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func (dc *DefaultController) BodyForm() (err error) {
	err = dc.RequireBody(BODY_FORM)
	if err != nil {
		return
	}
	dc.JSON(http.StatusOK, R{"uuid": dc.GetUUID(), "ip": dc.GetRemoteIP(), "URI": dc.GetRemoteURI(), "query": dc.GetFormValue("type_id")})
	return
}
func (dc *DefaultController) BodyFormPayload(payload struct {
	TypeId *int64 `type:"form" field:"type_id" validate:"required"`
}) (err error) {
	dc.JSON(http.StatusOK, R{"uuid": dc.GetUUID(), "query": payload.TypeId})
	return
}

func TestBodyFormEmpty(t *testing.T) {
	router := _Load(t)
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping", nil)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	handler := http.Handler(NewHandler(router.routes[5], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}
func TestBodyFormEmptyInvalidContentType(t *testing.T) {
	router := _Load(t)
	data := url.Values{}
	data.Set("type_id", "1")
	data.Add("title", "Hello world")

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping", bytes.NewBufferString(data.Encode()))
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", "")
	handler := http.Handler(NewHandler(router.routes[5], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}
func TestBodyFormValidPayload(t *testing.T) {
	router := _Load(t)

	data := url.Values{}
	data.Set("type_id", "1")

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping", bytes.NewBufferString(data.Encode()))
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	handler := http.Handler(NewHandler(router.routes[10], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	assert.Contains(t, string(body), "\"query\":1")
}
func TestBodyFormValid(t *testing.T) {
	router := _Load(t)

	data := url.Values{}
	data.Set("type_id", "1")
	data.Add("title", "Hello world")

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping", bytes.NewBufferString(data.Encode()))
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	handler := http.Handler(NewHandler(router.routes[5], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

//func TestBodyFormInvalid(t *testing.T) {
//	router := _Load(t)
//
//	data := url.Values{}
//	data.Set("type_id", "1")
//	data.Add("title", "")
//
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/ping", bytes.NewBufferString(data.Encode()))
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//
//	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
//	handler := http.Handler(NewHandler(router.routes[5], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want %v",
//			status, http.StatusOK)
//	}
//	assert.Contains(t, string(body), `API_FORM_VALIDATOR_FAIL`)
//	assert.Contains(t, string(body), `Request query validator found 1 error(s)`)
//}
