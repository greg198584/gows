package router

import (
	"context"
	"encoding/json"
	"encoding/xml"
	"github.com/twinj/uuid"
	"gitlab.com/greg198584/gows/promise"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"runtime"
	"runtime/debug"
)

type key int

const (
	// ParamsKey is the key in a request context.
	ParamsKey key = iota
)

type R map[string]interface{}

const (
	BODY_UNKNOWN        = iota
	BODY_JSON           = 1
	BODY_MUTLIPART_FORM = 2
	BODY_FORM           = 3
	BODY_FILE           = 4
	BODY_XML            = 5
	DEFAULT_LOCALE      = "fr_FR"
)

type ContextLast struct {
	File   string
	Line   int
	Caller *runtime.Func
}
type Context struct {
	context.Context

	typeBody      int
	uuid          uuid.UUID
	locale        string
	header        http.Header
	requestHeader http.Header
	statusCode    int
	data          interface{}
	query         map[string][]string
	queryParams   params
	body          *io.ReadCloser
	form          *url.Values
	multipartForm *multipart.Form
	//file          *File
	matchPath   string
	path        string
	contextData map[string]interface{}
	remoteip    string
	remoteuri   string
	w           http.ResponseWriter
	profils     []string
	url         *url.URL
}

func NewContext(ctx context.Context, header http.Header) *promise.Promise {
	c := new(Context)
	c.uuid = uuid.NewV4()
	c.header = header
	c.Context = context.WithValue(ctx, "uuid", c.uuid.String())
	c.contextData = make(map[string]interface{})
	c.SetRequestHeader(header)

	return promise.Resolve(c.Context, c)
}

func SetStatus(statusCode int, w http.ResponseWriter) {
	w.WriteHeader(statusCode)
	return
}

func SetJsonData(v interface{}, w http.ResponseWriter) {
	json.NewEncoder(w).Encode(v)
	return
}

func (c *Context) SetURL(url *url.URL) {
	c.url = url
}
func (c *Context) GetURL() (url *url.URL) {
	return c.url
}
func (c *Context) _setTypeBody(typeBody int) {
	c.typeBody = typeBody
}
func (c *Context) SetHeader(key string, value string) {
	c.header.Add(key, value)
}
func (c *Context) SetContextData(key string, data interface{}) {
	c.contextData[key] = data
}
func (c *Context) GetContextData(key string) interface{} {
	return c.contextData[key]
}
func (c *Context) GetHeader() http.Header {
	return c.requestHeader
}
func (c *Context) GetContext() *Context {
	return c
}
func (c *Context) RequireBody(typeBody int) (err error) {
	if c.typeBody != typeBody {
		want := ""
		switch typeBody {
		case BODY_FILE:
			want = "application/octet-stream"
			break
		case BODY_JSON:
			want = "application/json"
			break
		case BODY_FORM:
			want = "application/x-www-form-urlencoded"
			break
		case BODY_MUTLIPART_FORM:
			want = "multipart/form-data"
			break
		}
		err = HTTPBadRequest().
			SetCode(API_BODY_TYPE_FAIL).
			SetMessage("The Content-Type is incorrect for this method").
			SetDetails(map[string]interface{}{"got": c.requestHeader.Get("Content-Type"), "want": want})
		return
	}
	return nil
}
func (c *Context) GetJSON() interface{} {
	return c.data
}
func (c *Context) _SetLocale() {
	loc := c.requestHeader.Get("Accept-Language")
	if loc == "" {
		c.SetLocale(DEFAULT_LOCALE)
		return
	}
	c.SetLocale(loc)
}
func (c *Context) SetLocale(language string) {
	c.locale = language
}
func (c *Context) SetMatchPath(path string) {
	c.matchPath = path
}
func (c *Context) SetProfils(profils []string) {
	c.profils = profils
}
func (c *Context) GetProfils() []string {
	return c.profils
}
func (c *Context) GetMatchPath() string {
	return c.matchPath
}
func (c *Context) GetPath() string {
	return c.path
}
func (c *Context) GetLocale() string {
	return c.locale
}
func (c *Context) IsFinish() bool {
	return c.statusCode != 0
}
func (c *Context) JSON(statusCode int, data interface{}) {
	c.statusCode = statusCode
	c.data = data
}
func (c *Context) SetRequestHeader(values http.Header) {
	c.requestHeader = values
	c._SetLocale()
}
func (c *Context) SetRemote(ip string, uri string) {
	c.remoteip = ip
	c.remoteuri = uri
}
func (c *Context) SetQuery(query map[string][]string, params params, path string) {
	c.path = path
	c.query = query
	c.queryParams = params
}

//func (c *Context) SetFile(body io.ReadCloser) {
//	c.file = new(File)
//	c.file.data = body
//
//	c._setTypeBody(BODY_FILE)
//}
func (c *Context) SetJSON(body io.ReadCloser) {
	c._setTypeBody(BODY_JSON)
	c.body = &body
}
func (c *Context) SetXML(body io.ReadCloser) {
	c._setTypeBody(BODY_XML)
	c.body = &body
}
func (c *Context) SetForm(form url.Values) {
	c._setTypeBody(BODY_FORM)
	c.form = &form
}
func (c *Context) SetMultiPartForm(multipartForm *multipart.Form) {
	c._setTypeBody(BODY_MUTLIPART_FORM)
	c.multipartForm = multipartForm
}
func (c *Context) GetFormValue(value string) string {
	if c.form == nil {
		return ""
	}
	return c.form.Get(value)
}

//func (c *Context) GetFile() *File {
//	return c.file
//}

func (c *Context) GetBody() *io.ReadCloser {
	return c.body
}
func (c *Context) GetMultiFormFormValue(value string) string {
	if c.multipartForm == nil {
		return ""
	}
	if c.multipartForm.Value[value] == nil {
		return ""
	}
	return c.multipartForm.Value[value][0]
}
func (c *Context) GetMultiFormFormFile(value string) *multipart.FileHeader {
	if c.multipartForm == nil {
		return nil
	}
	if _, ok := c.multipartForm.File[value]; !ok {
		return nil
	}
	return c.multipartForm.File[value][0]
}
func (c *Context) ParseBody(v interface{}) (err error) {
	if c.body == nil {
		// err = io.EOF
		err = HTTPBadRequest().
			SetCode(API_BODY_EOF).
			SetMessage("The body of the request is empty").
			SetTrace(string(debug.Stack()))
		return
	}
	switch c.typeBody {
	case BODY_JSON:
		dec := json.NewDecoder(*c.body)
		err = dec.Decode(&v)
		if err == io.EOF {
			err = HTTPBadRequest().
				SetCode(API_BODY_EOF).
				SetMessage("The body of the request is empty").
				SetTrace(string(debug.Stack()))
		}
		if err == io.ErrUnexpectedEOF {
			err = HTTPBadRequest().
				SetCode(API_BODY_JSON_PARSE_FAIL).
				SetMessage("The body of the request contains badly-formed JSON").
				SetTrace(string(debug.Stack()))
		}

	case BODY_XML:
		dec := xml.NewDecoder(*c.body)
		err = dec.Decode(&v)
		if err == io.EOF {
			err = HTTPBadRequest().
				SetCode(API_BODY_EOF).
				SetMessage("The body of the request is empty").
				SetTrace(string(debug.Stack()))
		}
		if err == io.ErrUnexpectedEOF {
			err = HTTPBadRequest().
				SetCode(API_BODY_XML_TYPE_FAIL).
				SetMessage("The body of the request contains badly-formed JSON").
				SetTrace(string(debug.Stack()))
		}
	}

	return
}

func (c *Context) GetQuery() map[string][]string {
	return c.query
}
func (c *Context) GetQueryParams() params {
	return c.queryParams
}
func (c *Context) GetParam(name string) string {
	for i := range c.queryParams {
		if c.queryParams[i].key == name {
			return c.queryParams[i].value
		}
	}
	return ""
}
func (c *Context) StatusCode() int {
	return c.statusCode
}
func (c *Context) Response() interface{} {
	return c.data
}
func (c *Context) GetUUID() string {
	return c.uuid.String()
}
func (c *Context) GetRemoteIP() string {
	return c.remoteip
}
func (c *Context) GetRemoteURI() string {
	return c.remoteuri
}
func (c *Context) GetResponseWriter() http.ResponseWriter {
	return c.w
}

//func NewContext() *Context {
//	return &Context{}
//}

// GetParam gets parameters from request.
//func GetParam(ctx context.Context, name string) string {
//	params, _ := ctx.Value(BODY_UNKNOWN).(params)
//
//
//
//	return ""
//}
