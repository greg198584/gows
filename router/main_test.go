package router

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/greg198584/gows/config"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"syscall"
	"testing"
	"time"
)

func init() {
	err := os.Setenv("ENV", "DEV")
	if err != nil {
		log.Fatal(err)
	}
	err = os.Setenv("DISABLE_HTTP_LISTEN", "1")
	if err != nil {
		log.Fatal(err)
	}
	err = os.Setenv("CFG_PATH", "./__TEST_CI")
	if err != nil {
		log.Fatal(err)
	}
	config.Reset()
}
func (uc *DefaultController) GraceFull() (err error) {
	tG.Logf("called GraceFull, waiting 3 second before sending response")
	time.Sleep(3 * time.Second)
	uc.JSON(200, R{
		"message": "Hello GraceFull",
	})
	tG.Logf("http response sended")
	return
}
func (uc *DefaultController) Panic() (err error) {
	panic("panic")
}
func (uc *DefaultController) Timeout() (err error) {
	time.Sleep(1 * time.Second)
	return
}
func TestNew(t *testing.T) {
	router := New(&ApiController{})
	err := router._Load()
	log.Print(err)

	status := assert.Nil(t, router.Error())
	if !status {
		return
	}
	assert.NotNil(t, router)

	assert.NotZero(t, len(router.payload))
	assert.Equal(t, router.routes[0].method, "POST")
	assert.Equal(t, router.routes[0].pattern, "/api/ping")

	t.Logf("test is invoke %s@%s", "DefaultController", "Ping")

	c, err := NewContext(context.Background(), nil).Await()
	assert.Nil(t, err)
	err = router.routes[0]._IsValidInvoke(c.(*Context))
	assert.Nil(t, err)
}
func TestNewReturnNilController(t *testing.T) {
	router := New(&ApiController{})
	err := router._Load()
	assert.Nil(t, err)
	assert.NotNil(t, router)

	route := route{}
	route.controllerName = "DefaultControllerFail"
	route.methodName = "Ping"
	route.router = *router

	c, err := NewContext(context.Background(), nil).Await()
	assert.Nil(t, err)
	err = route._IsValidInvoke(c.(*Context))
	t.Logf("test is fail invoke %s@%s", "DefaultControllerFail", "Ping")
	pass := assert.NotNil(t, err)
	if pass {
		assert.Contains(t, err.Error(), "*router.ApiController@DefaultControllerFail method does not return your Controller")
	}
}
func TestNewReturnControllerEmoty(t *testing.T) {
	router := New(&ApiController{})

	assert.Nil(t, router.Error())
	assert.NotNil(t, router)

	route := route{}
	route.controllerName = "DefaultControllerEmpty"
	route.methodName = "Ping"
	route.router = *router

	c, err := NewContext(context.Background(), nil).Await()
	assert.Nil(t, err)
	err = route._IsValidInvoke(c.(*Context))
	t.Logf("test is fail invoke %s@%s", "DefaultControllerEmpty", "Ping")
	pass := assert.NotNil(t, err)
	if pass {
		assert.Contains(t, err.Error(), "Ping does not inherit *router.DefaultControllerEmpty")
	}
}
func TestNewUnknownController(t *testing.T) {
	router := New(&ApiController{})
	err := router._Load()
	assert.Nil(t, err)
	assert.NotNil(t, router)

	route := route{}
	route.controllerName = "UnknownController"
	route.methodName = "Ping"
	route.router = *router

	c, err := NewContext(context.Background(), nil).Await()
	assert.Nil(t, err)
	err = route._IsValidInvoke(c.(*Context))
	t.Logf("test is fail invoke %s@%s", "UnknownController", "Ping")
	pass := assert.NotNil(t, err)
	if pass {
		assert.Contains(t, err.Error(), "your default controller *router.ApiController does not return UnknownController")
	}
}
func TestNewControllerBadReturn(t *testing.T) {
	router := New(&ApiController{})
	err := router._Load()
	assert.Nil(t, err)
	assert.NotNil(t, router)

	route := route{}
	route.controllerName = "DefaultController"
	route.methodName = "PingBadInput"
	route.router = *router

	c, err := NewContext(context.Background(), nil).Await()
	assert.Nil(t, err)
	err = route._IsValidInvoke(c.(*Context))
	t.Logf("test is fail invoke %s@%s", "DefaultController", "PingBadInput")
	pass := assert.NotNil(t, err)
	if pass {
		assert.Contains(t, err.Error(), "Invalid argument. got: ptr (*router.Context), want: struct")
	}
}
func TestNewControllerBadInputStruct(t *testing.T) {
	router := New(&ApiController{})

	assert.Nil(t, router.Error())
	assert.NotNil(t, router)

	route := route{}
	route.controllerName = "DefaultController"
	route.methodName = "PingBadReturn"
	route.router = *router

	c, err := NewContext(context.Background(), nil).Await()
	assert.Nil(t, err)
	err = route._IsValidInvoke(c.(*Context))
	t.Logf("test is fail invoke %s@%s", "DefaultController", "PingBadReturn")
	pass := assert.NotNil(t, err)
	if pass {
		assert.Contains(t, err.Error(), "DefaultController@PingBadReturn need to return a `error`")
	}
}
func TestNewUnknownMethod(t *testing.T) {
	router := New(&ApiController{})

	assert.Nil(t, router.Error())
	assert.NotNil(t, router)

	route := route{}
	route.controllerName = "DefaultController"
	route.methodName = "pong"
	route.router = *router

	c, err := NewContext(context.Background(), nil).Await()
	assert.Nil(t, err)
	err = route._IsValidInvoke(c.(*Context))
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "Pong does not inherit *router.DefaultController")
}
func TestNewLowerNameMethod(t *testing.T) {
	router := New(&ApiController{})
	err := router._Load()

	assert.Nil(t, err)
	assert.NotNil(t, router)

	assert.NotZero(t, len(router.payload))
	assert.Equal(t, router.routes[0].method, "POST")
	assert.Equal(t, router.routes[0].pattern, "/api/ping")

	c, err := NewContext(context.Background(), nil).Await()
	assert.Nil(t, err)
	err = router.routes[0]._IsValidInvoke(c.(*Context))
	assert.Nil(t, err)
}
func TestInvoke(t *testing.T) {
	router := New(&ApiController{})
	err := router._Load()
	assert.Nil(t, err)
	assert.NotNil(t, router)

	assert.NotZero(t, len(router.payload))
	assert.Equal(t, router.routes[0].method, "POST")
	assert.Equal(t, router.routes[0].pattern, "/api/ping")

	t.Logf("invoke %s@%s", "DefaultController", "Ping")
	handler := router.routes[0]._Invoke()
	assert.NotNil(t, handler)
	assert.Equal(t, "func(*router.Context) *toolspromise.Promise", reflect.TypeOf(handler).String())
}

func TestInvokeWithMiddleware(t *testing.T) {
	router := New(&ApiController{}).
		AddMiddlewares(&AppMiddleware{})

	if !assert.NotNil(t, router) {
		return
	}
	err := router._Load()

	if !assert.Nil(t, err) {
		return
	}

	assert.NotZero(t, len(router.payload))
	assert.Equal(t, router.routes[0].method, "POST")
	assert.Equal(t, router.routes[0].pattern, "/api/ping")

	t.Logf("invoke %s@%s", "DefaultController", "Ping")
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping/:id", nil)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req = mux.SetURLVars(req, map[string]string{
		"id": "1",
	})
	handler := http.Handler(NewHandler(router.routes[0], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}
func TestInvokeWithMiddlewarePanic(t *testing.T) {
	err := os.Setenv("ENV", "DEV")
	if err != nil {
		log.Fatal(err)
	}
	router := New(&ApiController{}).
		AddMiddlewares(&AppMiddlewarePanic{})

	err = router._Load()

	assert.Nil(t, err)

	assert.NotZero(t, len(router.payload))
	assert.Equal(t, router.routes[0].method, "POST")
	assert.Equal(t, router.routes[0].pattern, "/api/ping")

	t.Logf("invoke %s@%s", "DefaultController", "Ping")
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/ping/:id", nil)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req = mux.SetURLVars(req, map[string]string{
		"id": "1",
	})
	handler := http.Handler(NewHandler(router.routes[0], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}
	assert.Contains(t, string(body), API_INTERNAL_ERROR)
	assert.Contains(t, string(body), "Trace")
}
func TestOtherController(t *testing.T) {
	r := New(&ApiController{}).
		AddRoute(map[string]interface{}{"handler": "OtherController@Hello", "path": "GET /api/other/hello"})
	err := r._Load()
	log.Print(err)
}

var tG *testing.T

func _close(t *testing.T, timeout time.Duration) {
	t.Logf("waiting %d second before send SIGTERM", timeout)
	time.Sleep(timeout * time.Second)
	t.Logf("sending SIGTERM ")
	err := syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
	assert.Nil(t, err)
}
func TestPayload_Listen(t *testing.T) {
	_ = os.Unsetenv("DISABLE_HTTP_LISTEN")
	go _close(t, 2)
	t.Logf("http listen")
	err := New(&ApiController{}).
		_Load()

	assert.Nil(t, err)
	t.Logf("end of listening with gracefull")
}
func TestPayload_ListenGraceFull(t *testing.T) {
	tG = t
	go func() {
		time.Sleep(2 * time.Second)
		t.Logf("call api/gracefull")
		go _close(t, 1)
		go func() {
			time.Sleep(2 * time.Second)
			_, err := http.Get("http://127.0.0.1:8210/api/gracefull")
			assert.NotNil(t, err)
			t.Logf("server closed -> %s", err.Error())
		}()
		go func() {
			assert.Equal(t, 0, Get().GetActiveCon())
			time.Sleep(2 * time.Second)
			assert.Equal(t, 1, Get().GetActiveCon())
		}()
		resp, err := http.Get("http://127.0.0.1:8210/api/gracefull")
		assert.Nil(t, err)
		assert.Equal(t, 200, resp.StatusCode)

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			assert.Nil(t, err)
			return
		}

		t.Logf("receive response : %s", string(body))
		assert.Equal(t, 0, Get().GetActiveCon())
	}()

	err := New(&ApiController{}).
		_Load()

	assert.Nil(t, err)
}

//func TestPayload_ListenGraceFullServerClose(t *testing.T) {
//	tG = t
//	done := make(chan bool, 1)
//	go func() {
//		time.Sleep(2 * time.Second)
//		t.Logf("call api/gracefull")
//		_close(t, 0)
//		_, err := http.Get("http://127.0.0.1:8888/api/gracefull")
//		assert.NotNil(t, err)
//		t.Logf("server closed -> %s", err.Error())
//		done <- true
//	}()
//	err := New(&ApiController{}).
//		_Load()
//
//	assert.Nil(t, err)
//	select {
//	case <-done:
//	}
//}
func TestRouteGetPatternWithHttpPathPrefix(t *testing.T) {
	cfgPath := os.Getenv("CFG_PATH")
	os.Setenv("CFG_PATH", fmt.Sprintf("%s/http_with_path", cfgPath))
	config.Reset()
	defer os.Setenv("CFG_PATH", cfgPath)
	defer config.Reset()

	router := New(&ApiController{})
	err := router._Load()

	assert.Nil(t, err)
	assert.NotNil(t, router)
	assert.Equal(t, router.routes[0].GetPattern(), "/my-custom-api-prefix/api/ping")

}

func TestRouteGetPatternWithoutHttpPathPrefix(t *testing.T) {
	router := New(&ApiController{})
	err := router._Load()

	assert.Nil(t, err)
	assert.NotNil(t, router)
	assert.Equal(t, router.routes[0].GetPattern(), "/api/ping")
}

func TestRouteGetPathWithHttpPathPrefix(t *testing.T) {
	cfgPath := os.Getenv("CFG_PATH")
	os.Setenv("CFG_PATH", fmt.Sprintf("%s/http_with_path", cfgPath))
	config.Reset()
	defer os.Setenv("CFG_PATH", cfgPath)
	defer config.Reset()

	router := New(&ApiController{})
	err := router._Load()

	assert.Nil(t, err)
	assert.NotNil(t, router)
	path, err := router.routes[1].GetPath(1, "test")
	assert.Nil(t, err)
	assert.Equal(t, path, "/my-custom-api-prefix/api/ping/1/view/test")
}

func TestGetRouteByIdReturnsRouteWhenIdIsFound(t *testing.T) {
	router := New(&ApiController{})
	err := router._Load()

	assert.Nil(t, err)
	assert.NotNil(t, router)

	assert.NotZero(t, len(router.payload))
	route, err := router.GetRouteById("PostApiPing")
	assert.Nil(t, err)

	assert.Equal(t, route.method, "POST")
	assert.Equal(t, route.pattern, "/api/ping")
}

func TestGetRouteByIdReturnsAnErrorWhenNoRouteIsFound(t *testing.T) {
	router := New(&ApiController{})
	err := router._Load()

	assert.Nil(t, err)
	assert.NotNil(t, router)

	assert.NotZero(t, len(router.payload))
	_, err = router.GetRouteById("UnknownRouteId")
	assert.NotNil(t, err)
}

/*
	BENCHMARK ZONE !!!
*/
func BenchmarkInvoke(b *testing.B) {
	router := New(&ApiController{})

	if router.Error() != nil {
		return
	}
	for i := 0; i < b.N; i++ {
		_ = router.routes[0]._Invoke()
	}
}
