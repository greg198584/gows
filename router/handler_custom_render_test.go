package router

import (
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSimpleHandlerCustomRender(t *testing.T) {
	router := _Load(t)
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/api/json", nil)
	res := assert.Nil(t, err)
	if !res {
		return
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1",
	})
	handler := http.Handler(NewHandler(router.routes[0], new(TestJSONRender)))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want: %v",
			status, http.StatusOK)
	}
	assert.Contains(t, string(body), `{"message":"Hello World"}`)
}
