package router

import (
	"net/http"
	"strings"
)

type ErrorMsg struct {
	Code    int    `json:"err_code"`
	Message string `json:"err_message"`
}

func SetError(statusCode int, message string) ErrorMsg {
	errorMsg := ErrorMsg{
		Code:    statusCode,
		Message: message,
	}
	return errorMsg
}

type HTTPGenericError interface {
	Error() string
	Code() string
	StatusCode() int
	Message() string
	Details() interface{}
	Trace() []string
	OnDev() bool
}

type ErrorStackTrace interface {
	Error() string
	Trace() []string
}

type HTTPError struct {
	message    string
	code       string
	statusCode int
	details    interface{}
	trace      []string
	private    bool
}

func NewHTTPError(statusCode int) *HTTPError {
	return (new(HTTPError)).SetStatusCode(statusCode)
}
func HTTPBadRequest() *HTTPError {
	return (new(HTTPError)).SetStatusCode(http.StatusBadRequest)
}
func HTTPRequestTimeout() *HTTPError {
	return (new(HTTPError)).SetStatusCode(http.StatusRequestTimeout)
}
func HTTPInternalServerError() *HTTPError {
	return (new(HTTPError)).SetStatusCode(http.StatusInternalServerError)
}
func HTTPUnauthorized() *HTTPError {
	return (new(HTTPError)).SetStatusCode(http.StatusUnauthorized)
}
func (e *HTTPError) SetStatusCode(statusCode int) *HTTPError {
	e.statusCode = statusCode
	return e
}
func (e *HTTPError) SetCode(code string) *HTTPError {
	e.code = code
	return e
}
func (e *HTTPError) SetDetails(details interface{}) *HTTPError {
	e.details = details
	return e
}
func (e *HTTPError) SetMessage(message string) *HTTPError {
	e.message = message
	return e
}
func (e *HTTPError) SetOnDev(private bool) *HTTPError {
	e.private = private
	return e
}
func (e *HTTPError) SetTrace(trace string) *HTTPError {
	stack := strings.Replace(trace, "\t", "", -1)
	stackSlice := strings.Split(stack, "\n")

	var debug = false
	if strings.Contains(trace, "debug.Stack") {
		debug = true
	}
	for k, v := range stackSlice {
		if strings.Contains(v, "stack.go") && debug {
			stackSlice = stackSlice[k+5:]
			break
		}
		if strings.Contains(v, "panic") {
			stackSlice = stackSlice[k+2:]
			break
		}
	}
	e.trace = stackSlice
	return e
}
func (e *HTTPError) Error() string {
	return e.message
}
func (e *HTTPError) Details() interface{} {
	return e.details
}
func (e *HTTPError) Code() string {
	return e.code
}
func (e *HTTPError) Message() string {
	return e.message
}
func (e *HTTPError) StatusCode() int {
	if e.statusCode == 0 {
		return http.StatusInternalServerError
	}
	return e.statusCode
}
func (e *HTTPError) Trace() []string {
	if len(e.trace) == 0 {
		return []string{}
	}
	return e.trace
}
func (e *HTTPError) OnDev() bool {
	return e.private
}
