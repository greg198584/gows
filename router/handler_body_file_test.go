package router

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

//func TestBodyFileEmptyBody(t *testing.T) {
//	router := _Load(t)
//
//	rr := httptest.NewRecorder()
//	req, err := http.NewRequest("POST", "/api/ping", nil)
//	res := assert.Nil(t, err)
//	if !res {
//		return
//	}
//
//	req.Header.Set("Content-Type", "application/octet-stream")
//	handler := http.Handler(NewHandler(router.routes[7], nil))
//	handler.ServeHTTP(rr, req)
//
//	body, err := ioutil.ReadAll(rr.Body)
//	if err != nil {
//		assert.Nil(t, err)
//		return
//	}
//
//	PrintJsonResponse(t, body)
//	if status := rr.Code; status != http.StatusBadRequest {
//		t.Errorf("handler returned wrong status code: got %v want %v",
//			status, http.StatusBadRequest)
//	}
//	assert.Contains(t, string(body), API_BODY_EOF)
//}
func TestPanic(t *testing.T) {
	_ = os.Unsetenv("ENV")
	router := _Load(t)

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/api/panic", nil)
	res := assert.Nil(t, err)
	if !res {
		return
	}
	//for index, _ := range(router.routes) {
	//	log.Printf(fmt.Sprintf("routes [%d]", index))
	//}
	//return
	handler := http.Handler(NewHandler(router.routes[14], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
	assert.Contains(t, string(body), API_INTERNAL_ERROR)
	_ = os.Setenv("ENV", "DEV")
}
func TestTimeout(t *testing.T) {
	router := _Load(t)

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/api/timeout", nil)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	handler := http.Handler(NewHandler(router.routes[15], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusRequestTimeout {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
	assert.Contains(t, string(body), API_EXCEEDED_TIMEOUT)
}
func TestFailInvokeBodyFile(t *testing.T) {
	router := _Load(t)

	rr := httptest.NewRecorder()
	file, err := os.Open("./main.go")
	if err != nil {
		assert.Nil(t, err)
		return
	}
	defer file.Close()

	req, err := http.NewRequest("POST", "/api/fail", file)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", "application/octet-stream")
	handler := http.Handler(NewHandler(router.routes[8], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}
	assert.Contains(t, string(body), API_INVOKE_ERROR)
}
func TestFailInvokeBodyFileProduction(t *testing.T) {
	err := os.Unsetenv("ENV")
	router := _Load(t)

	rr := httptest.NewRecorder()
	file, err := os.Open("./main.go")
	if err != nil {
		assert.Nil(t, err)
		return
	}
	defer file.Close()

	req, err := http.NewRequest("POST", "/api/fail", file)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", "application/octet-stream")
	handler := http.Handler(NewHandler(router.routes[8], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}
	assert.Contains(t, string(body), API_INTERNAL_ERROR)
	_ = os.Setenv("ENV", "DEV")
}
func TestFailInvokeBodyFileProd(t *testing.T) {
	err := os.Unsetenv("ENV")
	assert.Nil(t, err)
	router := _Load(t)

	rr := httptest.NewRecorder()
	file, err := os.Open("./main.go")
	if err != nil {
		assert.Nil(t, err)
		return
	}
	defer file.Close()

	req, err := http.NewRequest("POST", "/api/fail", file)
	res := assert.Nil(t, err)
	if !res {
		return
	}

	req.Header.Set("Content-Type", "application/octet-stream")
	handler := http.Handler(NewHandler(router.routes[8], nil))
	handler.ServeHTTP(rr, req)

	body, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		assert.Nil(t, err)
		return
	}

	PrintJsonResponse(t, body)
	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusInternalServerError)
	}
	assert.Contains(t, string(body), API_INTERNAL_ERROR)
	_ = os.Setenv("ENV", "DEV")
}

//func TestBodyFile(t *testing.T) {
//	router := _Load(t)
//	ids := []int{7, 13}
//	for _, id := range ids {
//		rr := httptest.NewRecorder()
//		file, err := os.Open("./main.go")
//		if err != nil {
//			assert.Nil(t, err)
//			return
//		}
//		defer file.Close()
//
//		req, err := http.NewRequest("POST", "/api/ping", file)
//		res := assert.Nil(t, err)
//		if !res {
//			return
//		}
//
//		req.Header.Set("Content-Type", "application/octet-stream")
//
//		handler := http.Handler(NewHandler(router.routes[id], nil))
//		handler.ServeHTTP(rr, req)
//
//		body, err := ioutil.ReadAll(rr.Body)
//		if err != nil {
//			assert.Nil(t, err)
//			return
//		}
//
//		PrintJsonResponse(t, body)
//		if status := rr.Code; status != http.StatusOK {
//			t.Errorf("handler returned wrong status code: got %v want %v",
//				status, http.StatusOK)
//		}
//		assert.Contains(t, string(body), "uuid")
//	}
//}

func BenchmarkBodyFile(b *testing.B) {
	router := New(&ApiController{})
	if router.Error() != nil {
		return
	}
	file, err := os.Open("./main.go")
	if err != nil {
		assert.Nil(b, err)
		return
	}
	defer file.Close()

	for i := 0; i < b.N; i++ {
		rr := httptest.NewRecorder()
		req, err := http.NewRequest("POST", "/api/ping", file)
		if err != nil {
			return
		}

		req.Header.Set("Content-Type", "application/octet-stream")
		handler := http.Handler(NewHandler(router.routes[7], nil))
		handler.ServeHTTP(rr, req)
	}

}
