package router

import (
	"fmt"
	"gitlab.com/greg198584/gows/config"
	"gitlab.com/greg198584/gows/logger"
	"gitlab.com/greg198584/gows/promise"
	"net/http"
	"reflect"
	"runtime/debug"
	"strings"
)

type Handler interface {
	Next(c *Context) (err error)
}
type NextHandler struct {
	N func(c *Context) (err error)
}

func (n NextHandler) Next(c *Context) (err error) {
	err = n.N(c)
	return
}

type HandlerFunc func(c *Context) (err error)

func (h HandlerFunc) Next(c *Context) (err error) {
	return h(c)
}

type handler struct {
	route        route
	customRender JSONRender
	H            func(c *Context) *promise.Promise
}

func NewHandler(route route, customRender JSONRender) *handler {
	return &handler{route: route, customRender: customRender}
}

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}
func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	if code == 0 {
		return
	}
	lrw.ResponseWriter.WriteHeader(code)
}
func (h *handler) _Execute(c *Context, starting int) (p *promise.Promise) {
	if starting >= len(h.route.middleware) || len(h.route.middleware) == 0 {
		h.H(c).Then(func(data interface{}) interface{} {
			p = promise.Resolve(c, data)
			return data
		}).Catch(func(err error) error {
			p = promise.Reject(c, err)
			return nil
		})
		return
	}
	next := NextHandler{N: func(c *Context) (err error) {
		p = h._Execute(c, starting+1)
		return
	}}
	switch m := h.route.middleware[starting].(type) {
	case string:
		err := h.route._InvokeMiddleware(next, c, m, nil)
		if err != nil {
			p = promise.Reject(c, err)
			return
		}
		break
	case map[string]interface{}:
		keys := reflect.ValueOf(m).MapKeys()
		if len(keys) == 0 {
			p = h._Execute(c, starting+1)
			break
		}
		err := h.route._InvokeMiddleware(next, c, keys[0].String(), m[keys[0].String()])
		if err != nil {
			p = promise.Reject(c, err)
			return
		}
		break
	default:
		p = h._Execute(c, starting+1)
		break
	}
	return
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	lrw := NewLoggingResponseWriter(w)
	var render JSONRender
	NewContext(r.Context(), w.Header()).
		Then(func(data interface{}) interface{} {
			router := h.route.GetRouter()
			result, _ := router.tree.Search(r.Method, r.URL.Path)
			//logger.GetLog("router.VAR").Info(fmt.Sprintf("params = [%v]", result.params))
			//for i := range result.params {
			//	logger.GetLog("router.VAR").Info(fmt.Sprintf("key = [%s] - value = [%s]", result.params[i].key, result.params[i].value))
			//}
			uuid := data.(*Context).GetUUID()
			logger.SetUUID(uuid)
			logger.GetLog("router.handler").Info(fmt.Sprintf("path %v", result.path))

			data.(*Context).SetURL(r.URL)
			data.(*Context).SetMatchPath(fmt.Sprintf("%s %s", r.Method, result.path))
			data.(*Context).SetProfils(h.route.profils)
			w.Header().Add("X-Request-ID", uuid)
			data.(*Context).SetQuery(r.URL.Query(), result.params, result.path)
			data.(*Context).SetRemote(r.RemoteAddr, r.RequestURI)
			data.(*Context).w = w
			return data.(*Context)
		}).
		Then(func(data interface{}) interface{} {
			return NewRender(data.(*Context), h.customRender)
		}).
		Then(func(data interface{}) interface{} {
			render = data.(JSONRender)
			return render.SetResponseWriter(lrw)
		}).
		Then(func(data interface{}) interface{} {
			data.(JSONRender).SetRequest(r)
			return data.(JSONRender)
		}).
		Then(func(data interface{}) interface{} {
			c := data.(JSONRender).GetContext()
			c.SetRequestHeader(r.Header)
			maxSize := config.GetDefaultIntValue("http", "post_max_size", 1048576)
			var err error
			content := r.Header.Get("Content-Type")
			switch {
			case strings.Contains(content, "application/json"):
				if r.Body != nil {
					c.SetJSON(r.Body)
					break
				}
				err = HTTPBadRequest().
					SetCode(API_BODY_EOF).
					SetMessage("The body of the request is empty").
					SetTrace(string(debug.Stack()))
				break
			case strings.Contains(content, "application/xml"), strings.Contains(content, "application/soap+xml"):
				if r.Body != nil {
					c.SetXML(r.Body)
					break
				}
				err = HTTPBadRequest().
					SetCode(API_BODY_EOF).
					SetMessage("The body of the request is empty").
					SetTrace(string(debug.Stack()))
				break
			case content == "application/x-www-form-urlencoded":
				err = r.ParseForm()
				if err != nil {
					err = HTTPBadRequest().
						SetCode(API_BODY_FORM_EMPTY).
						SetMessage("The body of the request is empty")
					break
				}
				c.SetForm(r.Form)
				break
			case strings.Contains(content, "multipart/form-data"):
				err = r.ParseMultipartForm(maxSize)
				if err != nil {
					err = HTTPBadRequest().
						SetCode(API_BODY_FORM_DATA_EMPTY).
						SetMessage("The body (multipart/form-data) of the request is empty")
					break
				}
				c.SetMultiPartForm(r.MultipartForm)
				break
			case content == "application/octet-stream":
				if r.Body == nil {
					err = HTTPBadRequest().
						SetCode(API_BODY_EOF).
						SetMessage("The body of the request is empty").
						SetTrace(string(debug.Stack()))
					break
				}
				r.Body = http.MaxBytesReader(w, r.Body, maxSize)
				//c.SetFile(r.Body)
				break
			}
			if err != nil {
				return promise.Reject(c.Context, err)
			}
			return data.(JSONRender)
		}).
		Then(func(data interface{}) interface{} {
			c := data.(JSONRender).GetContext()
			return func() *promise.Promise {
				err := h.route._IsValidInvoke(c)
				if err != nil {
					logger.GetLog("http.router").Error(fmt.Sprintf("Request cannot be processed %s to %s %s@%s", r.URL.Path, h.route.Method(), h.route.controllerName, h.route.methodName))
					return promise.Reject(r.Context(), err)
				}
				logger.GetLog("http.router").Debug(fmt.Sprintf("[%s] Handling request %s %s %s CALL %s@%s", c.GetPath(), r.RemoteAddr, h.route.Method(), r.URL.Path, h.route.controllerName, h.route.methodName))
				return promise.Resolve(r.Context(), data)
			}()
		}).
		Then(func(data interface{}) interface{} {
			Get().AddCon()
			h.H = h.route._Invoke()

			return data.(JSONRender)
		}).
		Then(func(data interface{}) interface{} {
			c := data.(JSONRender).GetContext()
			//logger.GetLog("http.middleware").Debug(fmt.Sprintf("The request must go through %d middleware(s) [%v]", len(h.route.middleware), h.route.middleware))
			return h._Execute(c, 0)
		}).
		Then(func(data interface{}) interface{} {
			return render.Send()
		}).
		Catch(func(error error) error {
			if render == nil {
				logger.GetLog("router.handler").Error(fmt.Sprintf("erreur = [%s]", error.Error()))
				return nil
			}
			render.SetError(error)
			_, err := render.Send().Await()
			if err != nil {
				logger.GetLog("router.handler").Error(fmt.Sprintf("erreur = [%s]", err.Error()))
			}
			return nil
		})
	Get().RemoveCon()
}
