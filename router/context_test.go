package router

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewContext(t *testing.T) {
	ctx := context.Background()
	c := NewContext(ctx, map[string][]string{})

	assert.NotNil(t, c)
}
