package router

import (
	"encoding/json"
	"gitlab.com/greg198584/gows/promise"
	"net/http"
)

type TestJSONRender struct {
	err    error
	c      *Context
	w      http.ResponseWriter
	r      *http.Request
	custom *JSONRender
}

func (r *TestJSONRender) SetRequest(request *http.Request) {
	r.r = request
}

func (r *TestJSONRender) SetResponseWriter(w http.ResponseWriter) *promise.Promise {
	r.w = w
	return promise.Resolve(r.c, r)
}

func (r *TestJSONRender) SetError(err error) {
	r.err = err
}

func (r *TestJSONRender) SetContext(c *Context) {
	r.c = c
}
func (r *TestJSONRender) GetContext() *Context {
	return r.c
}

func (r *TestJSONRender) Send() *promise.Promise {
	r._Respond()

	return promise.Resolve(r.GetContext(), r)
}

func (r *TestJSONRender) _Respond() {
	r.w.Header().Add("X-Request-ID", r.c.GetUUID())
	r.w.WriteHeader(r.c.StatusCode())

	if r.c.Response() != nil {
		r.w.Header().Add("Content-Type", "application/json")
		err := json.NewEncoder(r.w).Encode(r.c.Response())

		if err != nil {
			r.w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}
