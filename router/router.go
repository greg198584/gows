package router

import (
	"errors"
	"net/http"
)

// Router represents the router which handles routing.
type Router struct {
	tree *tree
}

// route represents the route which has data for a routing.
type Route struct {
	methods []string
	path    string
	handler *handler
	profils []string
	//middlewares middlewares
}

type contextKey int

const (
	varsKey contextKey = iota
)

var (
	tmpRoute = &Route{}

	// Error for not found.
	ErrNotFound = errors.New("no matching route was found")
	// Error for method not allowed.
	ErrMethodNotAllowed = errors.New("methods is not allowed")
	// Configurable Handler to be used when no route matches.
	NotFoundHandler http.Handler
	// Configurable Handler to be used when the request method does not match the route.
	MethodNotAllowedHandler http.Handler
)

// NewRouter creates a new router.
func NewRouter() *Router {
	return &Router{
		tree: NewTree(),
	}
}

//// Use sets middlewares.
//func (r *Router) Use(mws ...middleware) *Router {
//	nm := NewMiddlewares(mws)
//	tmpRoute.middlewares = nm
//	return r
//}

func (r *Router) Methods(methods ...string) *Router {
	tmpRoute.methods = append(tmpRoute.methods, methods...)
	return r
}

func (r *Router) Profils(scopes ...string) *Router {
	tmpRoute.profils = append(tmpRoute.profils, scopes...)
	return r
}

// Handler sets a handler.
func (r *Router) Handler(path string, handler *handler) {
	tmpRoute.handler = handler
	tmpRoute.path = path
	r.Handle()
}

// Handle handles a route.
func (r *Router) Handle() {
	r.tree.Insert(tmpRoute.methods, tmpRoute.path, tmpRoute.handler)
	tmpRoute = &Route{}
}

//func handleErr(err error) int {
//	var status int
//	switch err {
//	case ErrMethodNotAllowed:
//		status = http.StatusMethodNotAllowed
//	case ErrNotFound:
//		status = http.StatusNotFound
//	}
//	return status
//}
