package router

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/greg198584/gows/config"
	"gitlab.com/greg198584/gows/logger"
	"gitlab.com/greg198584/gows/promise"
	"gitlab.com/greg198584/gows/tools"
	"net/http"
	"os"
	"os/signal"
	"reflect"
	"regexp"
	"runtime/debug"
	"strings"
	"sync"
	"syscall"
	"time"
)

const NAMESPACE = "router"

func init() {
}

type AppController interface {
	OnCreate() (err error)
	OnFinish() (err error)
}
type router struct {
	*sync.Mutex
	controller      []AppController
	middlewares     map[string]reflect.Value
	payload         []map[string]interface{}
	payloadManually []map[string]interface{}
	routes          []route
	myRouter        *Router
	middlewareList  []interface{}
	activeCon       int
	error           error
}

type route struct {
	router
	id             string
	method         string
	pattern        string
	controllerName string
	methodName     string
	middleware     []interface{}
	handler        *reflect.Value
	controller     reflect.Value
	profils        []string
}
type moduleController struct {
	signIn bool `json:"sign_in"`
	signUp bool `json:"sign_up"`
}

func (r *route) Method() string {
	return r.method
}
func (r *route) SetMethod(method string) {
	r.method = method
}
func (r *route) GetPattern() string {
	return r.pattern
}
func (r *route) GetPath(params ...interface{}) (path string, err error) {
	path = r.GetPattern()
	regex, err := regexp.Compile(":([A-Za-z0-9]+)")
	if err != nil {
		return
	}

	replacements := regex.FindAllString(path, -1)
	if len(params) != len(replacements) {
		return path, errors.New(
			fmt.Sprintf("err: path has %d parameters, only %d parameters given", len(replacements), len(params)),
		)
	}

	for i, replacementName := range replacements {
		path = strings.Replace(path, replacementName, fmt.Sprintf("%v", params[i]), 1)
	}

	return
}

const (
	TYPE_NONE               = iota
	TYPE_QUERY              = "query"
	TYPE_JSON               = "json"
	TYPE_FORM               = "form"
	TYPE_MULTI_PART         = "multipart"
	TYPE_MULTI_PART_FILE    = "multipart-file"
	TYPE_FILE               = "file"
	ERROR_INVOKE_MIDDLEWARE = "%s middleware method not found"
)

var instance *router

func New(controller AppController) *router {
	logger.Load(
		config.GetValue("http", "log_namespace").(string),
		config.GetValue("http", "log_level").(string),
		config.GetValue("http", "log_verbose").(bool),
	)
	instance = new(router)
	instance.Mutex = new(sync.Mutex)
	instance.myRouter = NewRouter()
	instance.controller = append(instance.controller, controller)
	instance._LoadDefaultMiddleware()
	instance.middlewares = make(map[string]reflect.Value)
	instance.AddMiddlewares(&Middleware{})
	return instance
}
func (r *router) Use(controller AppController) *router {
	r.controller = append(r.controller, controller)
	return instance
}

/* Static function */
func Get() *router {
	return instance
}

/* Public function */
func (r *router) Error() error {
	return r.error
}

func (r *router) _LoadModuleController() (err error) {
	var modules []map[string]interface{}
	err = config.Get("modules", &modules)
	if err != nil {
		return
	}
	for _, v := range modules {
		if _, ok := v["actif"]; ok {
			actif := v["actif"].(bool)
			if actif {
				r.AddRoute(v)
			}
		} else {
			err = errors.New("syntaxe module json incorrect")
			return
		}
	}
	return
}

/*
* @Description Launch http server
* @Require http_port in http.json config
 */
func (r *router) Listen() (err error) {
	err = r._LoadModuleController()
	if err != nil {
		return err
	}
	err = r._Load()
	if err != nil {
		return err
	}
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	//port := os.Getenv("API_PORT")
	port := config.GetValue("http", "listen").(string)
	if port == "" {
		logger.GetLog(NAMESPACE).Error("port not specified")
		os.Exit(1)
	}
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", port),
		Handler: r.GetRouter(),
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.GetLog(NAMESPACE).Error(err.Error())
		}
	}()
	ip, _ := tools.GetIP()
	logger.GetLog(NAMESPACE).Info(fmt.Sprintf("Listen on %s:%s", ip, port))
	<-done
	logger.GetLog(NAMESPACE).Info(fmt.Sprintf("End of listening on %s:%s", ip, port))

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer func() {
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		logger.GetLog(NAMESPACE).Error(fmt.Sprintf("Shutdown Failed: %+v", err.Error()))
		os.Exit(1)
	}
	logger.GetLog(NAMESPACE).Info("Restarting API ...")
	return
}

func (r *router) AddMiddlewares(middleware interface{}) *router {
	m := reflect.ValueOf(middleware)
	if !m.IsValid() {
		return r
	}
	typeof := reflect.TypeOf(middleware)
	for i := 0; i < m.NumMethod(); i++ {
		method := m.Method(i)
		methodName := typeof.Method(i).Name

		r.AddMiddleware(methodName, method.Interface())
	}
	return r
}

func (r *router) AddMiddleware(name string, middlewareFunc interface{}) *router {
	middleware := reflect.ValueOf(middlewareFunc)
	if !middleware.IsValid() {
		return r
	}
	prototype := strings.Split(middleware.String(), " ")
	if strings.Join(prototype[len(prototype)-3:], " ") != "func(router.Handler) router.Handler Value>" {
		return r
	}
	r.Lock()
	r.middlewares[name] = middleware
	r.Unlock()
	return r
}
func (r *router) GetActiveCon() int {
	return r.activeCon
}
func (r *router) AddCon() {
	r.activeCon++
}
func (r *router) RemoveCon() {
	r.activeCon--
}
func (r *router) GetRouter() *Router {
	return instance.myRouter
}
func (r *router) GetRoutes() []route {
	return r.routes
}
func (r *router) GetRouteById(id string) (route route, err error) {
	for _, route := range r.routes {
		if route.id == id {
			return route, err
		}
	}

	return route, errors.New(fmt.Sprintf("no route found with ID '%s'", id))
}
func (r *router) Apply() {
	for _, route := range r.routes {
		logger.GetLog(NAMESPACE).Info(fmt.Sprintf("route pattern = [%s]", route.pattern))
		if len(route.profils) == 0 {
			logger.GetLog(NAMESPACE).Warning(fmt.Sprintf("\t [ no_profils ]"))
		} else {
			for _, profil := range route.profils {
				logger.GetLog(NAMESPACE).Info(fmt.Sprintf("profil present [%s]", profil))
			}
		}
		r.myRouter.Methods(route.method).Handler(route.pattern, NewHandler(route, nil))
	}
}

/* Private function */
func (r *router) _LoadDefaultMiddleware() {
	m := config.GetValue("http", "middleware")
	if m == nil {
		return
	}
	if reflect.TypeOf(m) != reflect.TypeOf([]interface{}{}) {
		return
	}
	r.middlewareList = m.([]interface{})
}
func (r *router) AddRoute(route map[string]interface{}) *router {
	r.payloadManually = append(r.payloadManually, route)
	return r
}
func (r *router) _Load() (err error) {
	if err != nil {
		return err
	}
	err = config.Get("routes", &r.payload)
	if err != nil {
		return
	}
	if len(r.payload) == 0 {
		err = errors.New(fmt.Sprintf("no routes.json found into `%s` folder", os.Getenv("CFG_PATH")))
		return
	}
	r.payload = append(r.payload, r.payloadManually...)
	err = r._Parsing()
	if err != nil {
		return
	}
	r.Apply()
	return
}

func (r *router) LoadTest() (err error) {
	err = config.Get("routes", &r.payload)
	if err != nil {
		return
	}
	if len(r.payload) == 0 {
		err = errors.New(fmt.Sprintf("no routes.json found into `%s` folder", os.Getenv("CFG_PATH")))
		return
	}
	r.payload = append(r.payload, r.payloadManually...)
	err = r._Parsing()
	if err != nil {
		return
	}
	r.Apply()
	return
}

func (r *router) _Parsing() (err error) {
	for _, v := range r.payload {
		var id string
		if _, ok := v["id"]; ok {
			id = v["id"].(string)
		}
		if _, ok := v["path"]; !ok {
			return errors.New(fmt.Sprintf("path key is required for %+v", v))
		}
		if _, ok := v["handler"]; !ok {
			return errors.New(fmt.Sprintf("handler key is required for %+v", v))
		}
		var middlewareRoute []interface{}
		if _, ok := v["middleware"]; ok {
			middlewareRoute = v["middleware"].([]interface{})
		}
		var profils []interface{}
		var profilsList []string
		if _, ok := v["profils"]; ok {
			if reflect.TypeOf(v["profils"]) != reflect.TypeOf([]interface{}{}) {
				logger.GetLog(NAMESPACE).Warning("no_profils")
			} else {
				profils = v["profils"].([]interface{})
				for _, m := range profils {
					switch m.(type) {
					case string:
						profilsList = append(profilsList, m.(string))
					}
				}
			}

		}
		split := strings.Split(v["path"].(string), " ")
		if len(split) != 2 {
			continue
		}
		handler := strings.Split(v["handler"].(string), ".")
		if len(handler) != 2 {
			continue
		}

		// middleware parsing!
		var mergedMiddleware []interface{}
		found := map[string]int{}
		for _, m := range r.middlewareList {
			mergedMiddleware = append(mergedMiddleware, m)
			switch v := m.(type) {
			case string:
				found[v] = len(mergedMiddleware) - 1
			case map[string]interface{}:
				found[reflect.ValueOf(m).MapKeys()[0].String()] = len(mergedMiddleware) - 1
			}
		}
		for _, m := range middlewareRoute {
			switch v := m.(type) {
			case string:
				index, exist := found[v]
				if exist {
					mergedMiddleware[index] = v
				}
				continue
			case map[string]interface{}:
				index, exist := found[reflect.ValueOf(m).MapKeys()[0].String()]
				if exist {
					mergedMiddleware[index] = v
				}
				continue
			}
			mergedMiddleware = append(mergedMiddleware, m)
		}

		main_path := config.GetValue("http", "path").(string)
		route := route{}
		route.id = id
		route.SetMethod(split[0])
		route.pattern = main_path + split[1]
		route.controllerName = handler[0]
		route.methodName = handler[1]
		route.middleware = mergedMiddleware
		route.router = *r
		route.profils = profilsList
		r.routes = append(r.routes, route)
	}
	return
}

/*
* _InvokeMiddleware
* @description Utilisation de la reflection pour faire appel au bon middleware avec ses paramètres
 */
func (r *route) _InvokeMiddleware(next Handler, c *Context, name string, data interface{}) (err error) {
	var p *promise.Promise
	p = promise.New(c, func(resolve func(interface{}), reject func(error)) {
		r.Lock()
		m, ok := r.middlewares[name]
		r.Unlock()
		if !ok {
			reject(HTTPInternalServerError().
				SetCode(API_INVOKE_ERROR).
				SetMessage(fmt.Sprintf(ERROR_INVOKE_MIDDLEWARE, name)).
				SetTrace(string(debug.Stack())))
			return
		}
		if m.Type().NumIn() > 1 {
			reject(errors.New(">1"))
			return
		}
		argv := make([]reflect.Value, m.Type().NumIn())
		if m.Type().NumIn() == 1 && m.Type().In(0).Kind() != reflect.Struct {
			reject(errors.New(">1"))
			return
		}
		if m.Type().NumIn() == 1 && m.Type().In(0).Kind() == reflect.Struct {
			value := reflect.New(m.Type().In(0)).Interface()
			b, err := json.Marshal(data)
			if err == nil {
				_ = json.Unmarshal(b, &value)
			}
			argv[0] = reflect.Indirect(reflect.ValueOf(value))
		}
		result := m.Call(argv)
		h := result[0].Interface().(func(Handler) Handler)(next)
		err := h.Next(c)
		if err != nil {
			reject(err)
			return
		}
		resolve(nil)
	})

	data, err = p.Await()
	if err != nil {
		return
	}
	return
}
func (r *route) _Invoke() func(c *Context) *promise.Promise {
	return func(c *Context) (p *promise.Promise) {
		defer func() {
			if er := recover(); er != nil {
				p = promise.Reject(c.Context, HTTPInternalServerError().
					SetCode(API_INVOKE_ERROR).
					SetMessage(fmt.Sprintf("%s", er)).
					SetTrace(string(debug.Stack())).
					SetOnDev(true))
				logger.GetLog(NAMESPACE).Debug(string(debug.Stack()))
			}
		}()
		argv := make([]reflect.Value, r.handler.Type().NumIn())
		for i := range argv {
			if r.handler.Type().In(i).Kind() == reflect.Struct {
				t := r.handler.Type().In(i)
				payload := reflect.New(t)
				for i := 0; i < t.NumField(); i++ {
					field := t.Field(i)
					tag := field.Tag.Get("type")
					fieldName := field.Tag.Get("field")
					if fieldName != "" {
						field.Name = fieldName
					}
					switch tag {
					case TYPE_QUERY:
						value := c.GetQuery()[strings.ToLower(field.Name)]
						if field.Type.Kind() != reflect.String {
							toInt := tools.ToInt(value)
							if toInt == nil {
								p = promise.Reject(c.Context, nil)
								return
							}
							reflect.Indirect(payload).Field(i).Set(reflect.ValueOf(toInt))
							break
						}
						reflect.Indirect(payload).Field(i).Set(reflect.ValueOf(value))
					case TYPE_FORM:
						err := c.RequireBody(BODY_FORM)
						if err != nil {
							logger.GetLog(NAMESPACE).Error(fmt.Sprintf("erreur = [%s]", err.Error()))
							p = promise.Reject(c.Context, err)
							return
						}
						value := c.GetFormValue(strings.ToLower(field.Name))
						if field.Type.Kind() != reflect.String {
							reflect.Indirect(payload).Field(i).Set(reflect.ValueOf(tools.ToInt(value)))
							break
						}
						reflect.Indirect(payload).Field(i).Set(reflect.ValueOf(value))
						break
					case TYPE_JSON:
						err := c.RequireBody(BODY_JSON)
						if err != nil {
							logger.GetLog(NAMESPACE).Error(fmt.Sprintf("erreur = [%s]", err.Error()))
							p = promise.Reject(c.Context, err)
							return
						}
						data := reflect.Indirect(payload).Field(i).Type()
						value := reflect.New(data).Interface()
						err = c.ParseBody(&value)
						if err != nil {
							logger.GetLog(NAMESPACE).Error(fmt.Sprintf("erreur = [%s]", err.Error()))
							p = promise.Reject(c.Context, err)
							return
						}
						reflect.Indirect(payload).Field(i).Set(reflect.Indirect(reflect.ValueOf(value)))
						break
					case TYPE_MULTI_PART:
						err := c.RequireBody(BODY_MUTLIPART_FORM)
						if err != nil {
							logger.GetLog(NAMESPACE).Error(fmt.Sprintf("erreur = [%s]", err.Error()))
							p = promise.Reject(c.Context, err)
							return
						}
						value := c.GetMultiFormFormValue(strings.ToLower(field.Name))
						reflect.Indirect(payload).Field(i).Set(reflect.ValueOf(value))
						break
					case TYPE_MULTI_PART_FILE:
						err := c.RequireBody(BODY_MUTLIPART_FORM)
						if err != nil {
							logger.GetLog(NAMESPACE).Error(fmt.Sprintf("erreur = [%s]", err.Error()))
							p = promise.Reject(c.Context, err)
							return
						}
						value := c.GetMultiFormFormFile(strings.ToLower(field.Name))
						reflect.Indirect(payload).Field(i).Set(reflect.ValueOf(value))
						break
					case TYPE_FILE:
						//file := c.GetFile()
						//if file == nil {
						//	break
						//}
						//reflect.Indirect(payload).Field(i).Set(reflect.ValueOf(c.GetFile()))
						break
					}
				}
				//err := c.ValidateStruct(payload.Interface())
				//if err != nil {
				//	p = promise.Reject(c.Context, err)
				//	return
				//}
				argv[0] = reflect.Indirect(payload)
				continue
			}
		}
		appController := r.controller.Interface()
		switch g := appController.(type) {
		case AppController:
			err := g.OnCreate()
			if err != nil {
				logger.GetLog(NAMESPACE).Error(fmt.Sprintf("erreur = [%s]", err.Error()))
				promise.Reject(c.Context, err)
				return
			}
		}
		result := r.handler.Call(argv)
		switch g := appController.(type) {
		case AppController:
			err := g.OnFinish()
			if err != nil {
				logger.GetLog(NAMESPACE).Error(fmt.Sprintf("erreur = [%s]", err.Error()))
				promise.Reject(c.Context, err)
				return
			}
		}
		if result[0].Interface() == nil {
			p = promise.Resolve(c.Context, c)
			return
		}
		p = promise.Reject(c.Context, result[0].Interface().(error))
		return
	}
}
func (r *route) _IsValidInvoke(c *Context) (err error) {
	for _, template := range r.router.controller {
		err = nil
		typeof := reflect.Indirect(reflect.ValueOf(template))
		controller := reflect.New(typeof.Type())

		method := controller.MethodByName(r.controllerName)
		if !method.IsValid() {
			err = HTTPInternalServerError().
				SetCode(API_INVOKE_ERROR).
				SetMessage(fmt.Sprintf("your default controller %s does not return %s", reflect.ValueOf(controller.Interface()).Type().String(), r.controllerName)).
				SetTrace(string(debug.Stack())).
				SetOnDev(true)
			continue
		}
		reflect.Indirect(controller).Field(0).Set(reflect.ValueOf(c))
		result := method.Call(nil)
		if len(result) != 1 {
			err = HTTPInternalServerError().
				SetCode(API_INVOKE_ERROR).
				SetMessage(fmt.Sprintf("%s@%s method does not return your Controller", reflect.ValueOf(controller.Interface()).Type().String(), r.controllerName)).
				SetTrace(string(debug.Stack())).
				SetOnDev(true)
			continue
		}

		t := reflect.Indirect(result[0]).Field(0).Interface()
		switch t.(type) {
		case AppController:
			reflect.Indirect(result[0]).Field(0).Set(controller)
		default:
			err = HTTPInternalServerError().
				SetCode(API_INVOKE_ERROR).
				SetMessage(fmt.Sprintf("%s must inherit %s", r.controllerName, controller.Type().String())).
				SetTrace(string(debug.Stack())).
				SetOnDev(true)
			continue
		}
		r.methodName = strings.Title(r.methodName)
		method = reflect.ValueOf(result[0].Interface()).MethodByName(r.methodName)
		if !method.IsValid() {
			err = HTTPInternalServerError().
				SetCode(API_INVOKE_ERROR).
				SetMessage(fmt.Sprintf("%s does not inherit %s", r.methodName, result[0].Type().String())).
				SetTrace(string(debug.Stack())).
				SetOnDev(true)
			continue
		}
		if method.Type().NumIn() > 0 {
			for i := 0; i < method.Type().NumIn(); i++ {
				if method.Type().In(i).Kind() != reflect.Struct {
					err = HTTPInternalServerError().
						SetCode(API_INVOKE_ERROR).
						SetMessage(fmt.Sprintf("%s@%s Invalid argument. got: %v (%s), want: %v", r.controllerName, r.methodName, method.Type().In(i).Kind(), method.Type().In(i), "struct")).
						SetTrace(string(debug.Stack())).
						SetOnDev(true)
					continue
				}
			}
			if err != nil {
				continue
			}
		}
		handler := reflect.TypeOf(method.Interface())
		if handler.NumOut() == 0 || (handler.NumOut() == 1 && handler.Out(0).String() != "error") || handler.NumOut() > 1 {
			err = HTTPInternalServerError().
				SetCode(API_INVOKE_ERROR).
				SetMessage(fmt.Sprintf("%s@%s need to return a `error`", r.controllerName, r.methodName)).
				SetTrace(string(debug.Stack())).
				SetOnDev(true)
			continue
		}
		r.controller = controller
		r.handler = &method
		return
	}
	return
}

// ServeHTTP dispatches the request to the handler whose
// pattern most closely matches the request URL.
//func (rt *router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
//	method := req.Method
//	path := req.URL.Path
//	logger.GetLog("router.http").Info(fmt.Sprintf("[ %s ] [ %s ]", method, path))
//	result, _ := rt.myRouter.tree.Search(method, path)
//	logger.GetLog("router.http").Debug(fmt.Sprintf("h = [ %v ]", result))
//	h := result
//	//if result.actions.middlewares != nil {
//	//	h = result.actions.middlewares.then(result.actions.handler)
//	//}
//	if result.params != nil {
//		ctx := context.WithValue(req.Context(), ParamsKey, result.params)
//		req = req.WithContext(ctx)
//	}
//	h.ServeHTTP(w, req)
//}

func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	method := req.Method
	path := req.URL.Path
	logger.GetLog(NAMESPACE).Info(fmt.Sprintf("[ %s ] [ %s ]", method, path))
	result, err := r.tree.Search(method, path)
	if err != nil {
		status := handleErr(err)
		SetStatus(status, w)
		return
	}
	h := result.actions.handler
	if result.params != nil {
		ctx := context.WithValue(req.Context(), ParamsKey, result.params)
		req = req.WithContext(ctx)
	}
	h.ServeHTTP(w, req)
}

func handleErr(err error) int {
	var status int
	switch err {
	case ErrMethodNotAllowed:
		status = http.StatusMethodNotAllowed
	case ErrNotFound:
		status = http.StatusNotFound
	}
	return status
}
