package middlewares

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"gitlab.com/greg198584/gows/config"
	"gitlab.com/greg198584/gows/jwt"
	"gitlab.com/greg198584/gows/router"
	"gitlab.com/greg198584/gows/tools"
	"net/http"
	"strings"
	"time"
)

const (
	TOKEN_BAD_FORMAT_CODE       = "AUTHORIZATION_BAD_FORMAT"
	TOKEN_INVALID_CODE          = "AUTHORIZATION_INVALID"
	TOKEN_EXPIRED               = "AUTHORIZATION_EXPIRED"
	PROFIL_UNAUTHORIZED         = "AUTHORIZATION PROFIL INVALIDE"
	MESSAGE_TOKEN_INVALID_CODE  = "The token signature is incorrect"
	MESSAGE_TOKEN_INVALID_DATA  = "The token data is incorrect"
	MESSAGE_TOKEN_EXPIRED       = "Your token has expired"
	MESSAGE_PROFIL_UNAUTHORIZED = "Your scope has unauthorized"
	BY_PASS_BAD_FORMAT_ROUTE    = "By pass bad format route"
)

func Jwt(payload struct {
	Bypass []string `json:"bypass"`
}) func(h router.Handler) router.Handler {
	return func(h router.Handler) router.Handler {
		return router.HandlerFunc(func(c *router.Context) (err error) {
			var bypass = false
			for _, path := range payload.Bypass {
				path_split := strings.Split(path, " ")
				if len(path_split) != 2 {
					return router.NewHTTPError(http.StatusUnauthorized).
						SetCode(BY_PASS_BAD_FORMAT_ROUTE).
						SetMessage(BY_PASS_BAD_FORMAT_ROUTE).
						SetDetails(path_split)
				}
				main_path := config.GetValue("http", "path").(string)
				path = fmt.Sprintf("%s %s%s", path_split[0], main_path, path_split[1])
				if path == c.GetMatchPath() {
					bypass = true
				}
			}
			if bypass {
				return h.Next(c)
			}
			authorization := strings.Split(c.GetHeader().Get("Authorization"), " ")
			if len(authorization) != 2 {
				return router.NewHTTPError(http.StatusUnauthorized).
					SetCode(TOKEN_BAD_FORMAT_CODE).
					SetMessage(MESSAGE_TOKEN_INVALID_DATA).
					SetDetails(authorization)
			}
			tokens := strings.Split(authorization[1], ".")
			if len(tokens) != 3 {
				return router.NewHTTPError(http.StatusUnauthorized).
					SetCode(TOKEN_INVALID_CODE).
					SetMessage(MESSAGE_TOKEN_INVALID_DATA).
					SetDetails(authorization)
			}
			var payload []byte
			payload, err = base64.RawURLEncoding.DecodeString(tokens[1])
			if err != nil {
				return router.NewHTTPError(http.StatusUnauthorized).
					SetCode(TOKEN_INVALID_CODE).
					SetMessage(MESSAGE_TOKEN_INVALID_DATA).
					SetDetails(authorization)
			}
			var payloadData map[string]interface{}
			err = json.Unmarshal(payload, &payloadData)
			if err != nil {

				return router.NewHTTPError(http.StatusUnauthorized).
					SetCode(TOKEN_INVALID_CODE).
					SetMessage(MESSAGE_TOKEN_INVALID_DATA).
					SetDetails(authorization)
			}
			var data map[string]interface{}
			audEncrypted, ok := payloadData["aud"]
			if ok &&
				config.GetValue("http", "jwt_key").(string) != "" &&
				config.GetValue("http", "jwt_iv").(string) != "" {

				audDecrypted, err := _Decrypt(audEncrypted.(string), config.GetValue("http", "jwt_key").(string), config.GetValue("http", "jwt_iv").(string))
				if err != nil {
					return router.NewHTTPError(http.StatusUnauthorized).
						SetCode(TOKEN_BAD_FORMAT_CODE).
						SetMessage(MESSAGE_TOKEN_INVALID_DATA).
						SetDetails(authorization)
				}
				err = json.Unmarshal([]byte(strings.TrimSpace(audDecrypted)), &payloadData)
				if err != nil {
					return router.NewHTTPError(http.StatusUnauthorized).
						SetCode(TOKEN_BAD_FORMAT_CODE).
						SetMessage(MESSAGE_TOKEN_INVALID_DATA).
						SetDetails(authorization)
				}
				customerValue, ok := payloadData["customer"]
				if !ok {
					return router.NewHTTPError(http.StatusUnauthorized).
						SetCode(TOKEN_BAD_FORMAT_CODE).
						SetMessage(MESSAGE_TOKEN_INVALID_DATA).
						SetDetails(authorization)
				}
				data, err = jwt.VerifyUseKey(customerValue.(string), authorization[1])
				if err != nil {
					return router.NewHTTPError(http.StatusUnauthorized).
						SetCode(TOKEN_INVALID_CODE).
						SetMessage(MESSAGE_TOKEN_INVALID_CODE).
						SetDetails(authorization)
				}
				//pass := false
				//_, exist := payloadData["acl"]
				//if exist {
				//	logger.GetLog("middleware.jwt").Debug(fmt.Sprintf("IS EXIST payloadData [%v]", payloadData))
				//	aclList, ok := payloadData["acl"].([]interface{})
				//	if ok && len(aclList) > 0 {
				//		pass = false
				//		for _, acl := range aclList {
				//			if acl == c.GetHeader().Get("X-Real-Ip") {
				//				pass = true
				//			}
				//		}
				//	}
				//}

				//if !pass {
				//	logger.GetLog("middleware.jwt").Debug("ECHEC 5")
				//	return router.NewHTTPError(http.StatusUnauthorized).
				//		SetCode(TOKEN_INVALID_CODE).
				//		SetMessage(MESSAGE_TOKEN_INVALID_DATA).
				//		SetDetails(authorization)
				//}
				data = payloadData
			} else {

				data, err = jwt.Verify(authorization[1])
				if err != nil {
					return router.NewHTTPError(http.StatusUnauthorized).
						SetCode(TOKEN_INVALID_CODE).
						SetMessage(MESSAGE_TOKEN_INVALID_CODE).
						SetDetails(authorization)
				}
			}
			// if only exist!
			if data["exp"] != nil {
				i := tools.ToInt(data["exp"])
				if i == nil {
					return router.NewHTTPError(http.StatusUnauthorized).
						SetCode(TOKEN_EXPIRED).
						SetMessage(MESSAGE_TOKEN_EXPIRED).
						SetDetails(authorization)
				}
				tm := time.Unix(*i, 0)
				if time.Now().After(tm) {
					return router.NewHTTPError(http.StatusUnauthorized).
						SetCode(TOKEN_EXPIRED).
						SetMessage(MESSAGE_TOKEN_EXPIRED).
						SetDetails(map[string]interface{}{"exp": tm.String()})
				}
			}
			// gestion profil
			if data["profils"] != nil {
				var profilList []string
				profils := data["profils"].([]interface{})
				for _, m := range profils {
					switch m.(type) {
					case string:
						profilList = append(profilList, m.(string))
					}
				}
				profilsAuthorizedList := c.GetProfils()
				if len(profilsAuthorizedList) != 0 {
					authorizated_by_profil := false
					for _, profil := range profilList {
						for _, profil_valid := range profilsAuthorizedList {
							if profil == profil_valid {
								authorizated_by_profil = true
							}
						}
					}
					if authorizated_by_profil == false {
						return router.NewHTTPError(http.StatusUnauthorized).
							SetCode(PROFIL_UNAUTHORIZED).
							SetMessage(MESSAGE_PROFIL_UNAUTHORIZED).
							SetDetails(data["profils"])
					}
				}
			}
			c.SetContextData("Jwt", data)

			return h.Next(c)
		})
	}
}
func _Decrypt(encText string, keyHex string, ivHex string) (decrypted string, err error) {
	key, err := hex.DecodeString(keyHex)
	if err != nil {
		return
	}
	iv, err := hex.DecodeString(ivHex)
	if err != nil {
		return
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}
	text, err := base64.StdEncoding.DecodeString(encText)
	if err != nil {
		return
	}
	decryptedData := make([]byte, len(text))

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(decryptedData, text)
	decrypted = string(PKCS5UnPadding(decryptedData))
	return
}
func PKCS5UnPadding(src []byte) []byte {
	length := len(src)
	if length == 0 {
		return src
	}
	unpadding := int(src[length-1])
	return src[:(length - unpadding)]
}
