package middlewares

import "gitlab.com/greg198584/gows/router"

func Exemple() func(h router.Handler) router.Handler {
	return func(h router.Handler) router.Handler {
		return router.HandlerFunc(func(c *router.Context) (err error) {
			c.SetContextData("exemple", "middleware_exemple")
			return h.Next(c)
		})
	}
}
